import axios from "axios";

export default async function handler(req, res) {
    try {
        const response = await axios.get(
            "https://mos.esante.gouv.fr/NOS/JDV_J106-EnsembleProfession-RASS/FHIR/JDV-J106-EnsembleProfession-RASS/JDV_J106-EnsembleProfession-RASS-FHIR.json"
        );

        res.setHeader("Access-Control-Allow-Origin", "*"); // CORS header added
        res.status(200).json(response.data);
    } catch (error) {
        res.status(500).json({ error: "Failed to fetch data" });
    }
}

