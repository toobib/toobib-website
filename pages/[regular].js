import NotFound from "@layouts/404";
import Base from "@layouts/Baseof";
import Contact from "@layouts/Contact";
import Default from "@layouts/Default";
import Faq from "@layouts/Faq";
import Us from "@layouts/Us";
import Validation from "@layouts/ContactFormSuccess";
import ToobibPro from "@layouts/ToobibPro";
import Roadmap from "@layouts/Roadmap";
import Login from "@layouts/Login";
import Cotisations from "@layouts/Cotisations";
import Outils from "@layouts/Outils";
import { getRegularPage, getSinglePage } from "@lib/contentParser";
import Profile from "@layouts/Profile";
import ModifierProfile from "@layouts/ModifierProfile";
import UpdatedPassword from "@layouts//UpdatedPassword";
import Search from "@layouts/SearchResults";
import ToobibProfile from "@layouts/ToobibProfile";

// for all regular pages
const RegularPages = ({ data }) => {
  const { title, meta_title, description, image, noindex, canonical, layout } =
    data.frontmatter;
  const { content } = data;
  return (
    <Base
      title={title}
      description={description ? description : content.slice(0, 120)}
      meta_title={meta_title}
      image={image}
      noindex={noindex}
      canonical={canonical}
    >
      {layout === "404" ? (
        <NotFound data={data} />
      ) : layout === "toobib-profil" ? (
        <ToobibProfile data={data} />
      ) : layout === "connexion" ? (
        <Login data={data} />
      ) : layout === "mon-profil" ? (
        <Profile data={data} />
      ) : layout === "modifier-profil" ? (
        <ModifierProfile data={data} />
      ) : layout === "contact" ? (
        <Contact data={data} />
      ) : layout === "rechercher-un-professionnel-de-sante" ? (
        <Search data={data} />
      ) : layout === "roadmap" ? (
        <Roadmap data={data} />
      ) : layout === "toobib-pro" ? (
        <ToobibPro data={data} />
      ) : layout === "contact-form-success" ? (
        <Validation data={data} />
      ) : layout === "us" ? (
        <Us data={data} />
      ) : layout === "outils" ? (
        <Outils data={data} />
      ) : layout === "faq" ? (
        <Faq data={data} />
      ) : layout === "cotisations" ? (
        <Cotisations data={data} />
      ) : layout === "mdp-modifie" ? (
        <UpdatedPassword data={data} />
      ) : (
        <Default data={data} />
      )}
    </Base>
  );
};
export default RegularPages;

// for regular page routes
export const getStaticPaths = async () => {
  const allslugs = getSinglePage("content");
  const slugs = allslugs.map((item) => item.slug);
  const paths = slugs.map((slug) => ({
    params: {
      regular: slug,
    },
  }));

  return {
    paths,
    fallback: false,
  };
};

// for regular page data
export const getStaticProps = async ({ params }) => {
  const { regular } = params;
  const regularPage = await getRegularPage(regular);
  return {
    props: {
      slug: regular,
      data: regularPage,
    },
  };
};
