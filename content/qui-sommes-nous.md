---
title: Qui sommes-nous ?
layout: us
draft: false
infos: 
  - title: Qu'est-ce que Toobib ?
    description: Nous sommes une association qui œuvre pour des communs en santé, avec pour objectif de déployer une utilisation des données de santé éthique, transparente, respectueuse du droit à la protection des données personnelles dont les données couvertes par le secret médical sont une composante particulièrement sensible.<br>Toobib fait de la sensibilisation et de la formation auprès des professionnels de santé sur les enjeux du traitement des données de santé.<br>Toobib promeut, développe et met à disposition des outils, documents, guides, logiciels libres et open-sources par et pour les professionnel.le.s de santé.<br>Toobib regroupe notamment des professionnel.le.s de santé.



offers:
  - title: Pourquoi adhérer à Toobib ?
    users:
      - title: "S'équiper :"
        usages:
          - "Parce qu’aujourd’hui, un logiciel de santé est plus qu'un simple outil dans la pratique quotidienne du praticien. C'est devenu un pilier essentiel de la qualité des soins apportés aux patients."  
      - title: "Redevenir acteur :"
        usages:
          - "Parce qu'aujourd'hui, le logiciel de santé est monopolisé par de grands groupes privés. Le praticien ne peut que subir les choix de l’éditeur. Ils imposent leur prix, leurs décisions et leur vision de la santé"
      - title: "Reprendre confiance :"
        usages:
          - "Parce que le traitement des données de santé est un véritable enjeu éthique dont les professionnels de santé sont de plus en plus dépossédés"
history:
  title: Notre histoire
  desc: "Toobib c’est la fusion de deux entités, passionnées d’éthique et de liberté : Interhop et Dokos. <br><br>Interhop est une association qui regroupe des militants des logiciels libres et d’une utilisation auto-gérée des données de santé à l’échelle locale. Elle promeut et développe des logiciels libres et open-source pour la santé. <br><br>Dokos est une société très impliquée dans le domaine de l’économie social et solidaire, qui place l’éthique au coeur de son action.. Elle édite un logiciel de gestion open-source Dokos, une adaptation française d’ERPNext. Bénéficiant de plus de 10 ans de développements et d’utilisation dans des milliers d’entreprises, c’est un logiciel de gestion robuste et performant. Son implication dans le monde médical a commencé avec MAIA, un logiciel de gestion pour les sages-femmes qui est présent dans les cabinets libéraux depuis 2017. <br><br>En 2022, la route a réuni Dokos, Interhop mais aussi d’autres associations comme P4pillon. De discussions passionnées, de rêves ambitieux est né le beau projet de Toobib."
team:
  title: Notre équipe
  desc: "Toobib.org est une association française loi 1901 à but non lucratif. Notre équipe est composée de personnes passionnées qui se réunissent autour de valeurs communes.Professionnels de santé ou passionnés de technologies se réunissent autour d'un objetif commun : une plateforme de santé éthique."
  bureau:
  - icon: '/images/justice.svg'
    title: ALIBERT Juliette
    desc: Avocate
  - icon: '/images/midwife.svg'
    title: DECULTOT Céline 
    desc: Sage-femme
  members:
  - icon: '/images/midwife.svg'
    title: BARLIER Marlène
    desc: Sage-femme
  - icon: '/images/data-protection.svg'
    title: CHARLOT Chantal 
    desc: DPO et Infirmière
  - icon: '/images/code.svg'
    title: DECULTOT Charles-Henri
    desc: Développeur
  - icon: '/images/code.svg'
    title: DESGRIPPES Florent
    desc: Ingénieur
  - icon: '/images/code.svg'
    title: FORLER Corentin
    desc: Ingénieur
  - icon: '/images/doctor.svg'
    title: FOUGERAT Jérémie
    desc: Médecin
  - icon: '/images/doctor.svg'
    title: FROISSART Zoéline
    desc: Médecin
  - icon: '/images/doctor.svg'
    title: GENDRY Camille
    desc: Médecin
  - icon: '/images/code.svg'
    title: PARIS Nicolas
    desc: Ingénieur
  - icon: '/images/doctor.svg'
    title: PARROT Adrien
    desc: Médecin
  - icon: '/images/code.svg'
    title: PARROT Quentin
    desc: Ingénieur
  - icon: '/images/doctor.svg'
    title: PERRAUD Gabriel
    desc: Médecin
  - icon: '/images/pharmacien.svg'
    title: PRIOUX Antoine
    desc: Pharmacien
  - icon: '/images/code.svg'
    title: VERNEUIL Cyril
    desc: UI & UX Designer
  members_asso_title: Personnes morales
  members_asso:
  - icon: '/images/dokos.svg'
    title: Dokos
  - icon: '/images/association.svg'
    title: Interhop
  - icon: '/images/association.svg'
    title: P4pillon
  - icon: '/images/association.svg'
    title: SMG


cta:
  title: Nous soutenir 
  desc: Nous luttons pour protéger les données médicales. Si tu souhaites défendre une médecine mutualiste où le lien de confiance entre le patient et son médecin est respecté, aide nous.
  image: '/images/cta.svg' 
  button_solid:
    label: "Nous contacter"
    href: "/contact"
    rel: ""

headquarter:
  title: Siège social
  Adresse: Chez Céline Decultot, 7 bis allée Neuve 54520 LAXOU
email:
  text: contact@toobib.org
  url: mailto:contact@toobib.org

--- 
