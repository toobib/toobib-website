---
title: Cotisations
layout: cotisations
draft: false
articles:
  - title: Pourquoi cotiser ?
    descriptions:
      - text: "Toobib oeuvre pour des communs numériques en santé et promeut une utilisation des données de santé éthique et transparente, respectueuse du droit à la protection des données personnelles dont les données couvertes par le secret médical sont une composante particulièrement sensible.<br />
Toobib fait de la sensibilisation et de la formation auprès des professionnels de santé sur les enjeux du traitement des données de santé.<br />
Toobib.org promeut, développe et met à disposition des outils, documents, guides, logiciels libres et open-sources par et pour les professionnel.le.s de santé."
  - title: Par virement bancaire
    descriptions: 
      - text: "Des virements de banque à banque sont possibles sans frais : renseigne-toi auprès de ta banque !
<br />
Domiciliation : CRÉDIT COOPÉRATIF<br />
<br />
<b>Identification du compte pour une utilisation nationale</b><br />
Banque : 42559<br />
Guichet : 10000<br />
Compte : 08026979059<br />
Clé RIB : 32<br />
BIC : CCOPFRPPXXX<br />
<br />
<b>Identification du compte pour une utilisation internationale (IBAN)</b><br />
FR76 4255 9100 0008 0269 7905 932"
  - title: Via Hello Asso
    descriptions: 
      - text: 'helloasso'

---
