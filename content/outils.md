---
title: Outils
layout: outils
draft: false
articles:
  - title: ToobibRDV
    description: "ToobibRDV utilise le logiciel open-source Easy Appointment, une solution de gestion de rendez-vous en ligne. Avec ToobibRDV, vous pouvez optimiser la gestion de votre emploi du temps tout en garantissant la confidentialité des informations de vos patient.e.s."
    logo: "/../images/RDV.svg"
  - title: ToobibChat
    description: "ToobibChat utilise le logiciel opensource Element qui est une messagerie instantanée chiffrée basée sur le protocole Matrix. ToobibChat offre une interface conviviale pour que les professionnel.les de santé puissent échanger des messages, des appels audio/vidéo et des fichiers."
    logo: "/../images/messagerie.svg"
  - title: ToobibDrive
    description: "ToobibDrive utilise le logiciel CryptPad. C'est une suite d'outils de collaboration en ligne qui met l'accent sur la confidentialité des données. Grâce à un chiffrement de bout en bout, les professionnel.les de santé peuvent créer et partager des documents en toute sécurité."
    logo: "/../images/Drive.svg"
  - title: ToobibSecret
    description: "ToobibSecret utilise le logiciel open-source Bitwarden, un gestionnaire de mots de passe. ToobibSecret permet aux professionnel.les de santé de gérer et de protéger leurs identifiants et informations sensibles, vous pouvez ainsi stocker et générer des mots de passe en toute sécurité."
    logo: "/../images/Secret.svg"
  - title: ToobibTransfert
    description: "ToobibTransfert permet d'échanger des documents en toute sécurité."
    logo: "/../images/transfert.svg"
  - title: ToobibPro
    description: "ToobibPro est un logiciel opensource de gestion de patientèle qui aide les professionnel.les de la santé à gérer efficacement les données des patients, les rendez-vous, la facturation et la communication."
    logo: "/../images/toobibPro.svg"
  - title: ToobibAnnuaire  
    description: "ToobibAnnuaire référence et maintient à jour l'offre de santé française (établissement et professionnel.les de santé)"
    logo: "/../images/Miseajourdonnees.svg"
    url: https://carto.dev.toobib.org
  - title: ToobibDav
    description: "ToobibDav utilise le logiciel open-source EteSync, une solution de synchronisation et de partage de fichier CalDAV et CardDAV. ToobibDav permet aux professionnel.les de santé de synchroniser, accéder et partager leurs calendriers et leurs annuaires."
    logo: "/../images/Dav.svg"
  - title: Formation
    description: "L'association Toobib est une association d’éducation pour les professionnel.les de santé aux enjeux du numérique dans le domaine du soin."
    logo: "/../images/formation.svg"
---
