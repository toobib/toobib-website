---
title: Conditions Générales d'Utilisation
layout: cgu
draft: false
---

Les présentes conditions générales d’utilisation (dites « CGU ») ont pour objet l’encadrement juridique des modalités de mise à disposition de nos services ***HDS*** dont voici la liste exhaustive :
- [Toobib Drive](https://drive.toobib.org)
- [Toobib Chat](https://chat.toobib.org) 
- [Toobib Secret](https://secret.toobib.org) 
- [Toobib Authentification](https://auth.toobib.org/)
- [Toobib Pro](https://demo.toobib.org/) : version démo


Toute inscription ou utilisation de nos services ***HDS*** implique l’acceptation sans aucune réserve ni restriction des présentes CGU par l’utilisateur.

En cas de non-acceptation des CGU stipulées dans le présent "contrat", l’utilisateur se doit de renoncer à l’accès des services proposés par **« Toobib »**.

[Toobib](https://toobib.org/), pour améliorer ses services, se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes CGU.

Tous nos services reposent sur des logiciels opensources voire libres (FLOSS).

## Article 1 : Les mentions légales

Vous trouvez nos mentions légales [ici](https://toobib.org/mentions-legales/).

## Article 2 : Accès aux services

* **Toobib Drive** 

`Toobib Drive` utilise le logiciel opensource [CryptPad](https://cryptpad.fr) qui est une alternative aux services cloud propriétaires de type Drive. 

Ce logiciel est respectueux de la vie privée.

Tout le contenu stocké dans `Toobib Drive` est chiffré de bout-en-bout, i.e avant d’être envoyé, ce qui signifie que personne (même pas nous !) ne peut accéder à vos données à moins que vous ne lui donniez les clés .

*Pour y accéder :*  [drive.toobib.org](https://drive.toobib.org/)



* **Matrix** et **Element**

`Toobib Chat` utilise le logiciel opensource [element](https://element.io/) qui est une applications de chat sécurisée.

Elle vous permet de garder le contrôle de vos conversations, à l’abri de l’exploitation de données et des publicités.

Les échanges réalisés via `Toobib Chat` sont chiffrés de bout en bout dans le cadre du protocole d'échange opensource Matrix.

*Pour y accéder :* [chat.toobib.org](https://chat.toobib.org)

* **Gestionnaire de mots de passe**

`Toobib Secret` utilise le logiciel opensource [Bitwarden](https://bitwarden.com/fr-fr/). 

Un gestionnaire de mots de passe, c’est un coffre-fort virtuel pour stocker l'ensemble de ses mots de passe (banque, impôts, plateforme de formation, assurances, sites d’achat…).

*Pour y accéder :* [secret.toobib.org](https://secret.toobib.org/)

* **Keycloak**

`Toobib Authentification` utilise le logiciel opensource [keycloak](https://www.keycloak.org/) pour gérer l'authentification et les habilitations.

Keycloak est une solution open source de gestion des identités et des accès (IAM) qui offre des fonctionnalités de Single Sign-On (SSO) pour les applications web et mobiles. Il permet de centraliser la gestion des identités de vos utilisateurs et de contrôler l'accès à vos applications et services en ligne.

Au sein du Keycloak, Toobib s'interface avec les identités des professionnel.les de santé délivrées par [Pro Santé Connect](https://industriels.esante.gouv.fr/produits-et-services/pro-sante-connect/referentiel-psc) PSC. PSC est un téléservice mis en œuvre par l’Agence du Numérique en Santé (ANS) contribuant à simplifier l’identification électronique des professionnels intervenant en santé.

L’utilisateur peut se connecter grâce à son application mobile e-CPS ou sa carte CPS, avec un lecteur de cartes et les composants nécessaires.

*Pour y accéder :* [auth.toobib.org](https://auth.toobib.org)

* **Toobib Pro**

`Toobib Pro` utilise un fork de l'ERP  opensource [dokos.io](https://dokos.io/) qui est un dossier patient informatisé opensource .


*Pour y accéder :* [demo.toobib.org](https://demo.toobib.org)


Pour information, tous les frais supportés par l’utilisateur pour accéder au service (matériel informatique, connexion Internet, etc.) sont à sa charge.

Toobib s'efforce de maintenir le service en fonctionnement nominal. Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement, quel qu’il soit, et sous réserve de toute interruption ou modification en cas de maintenance, n’engage pas la responsabilité d'InterHop.

Vous avez besoin d’un de nos services, vous êtes utilisateur, vous avez la possibilité de nous contacter par messagerie électronique à l’adresse email suivante : [contact@toobib.org](mailto:contact@toobib.org)


## Article 3 : Collecte des données

Le site assure à l’utilisateur une collecte et un traitement d’informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, au RGPD et à [notre politique de confidentialité](https://toobib.org/politique-de-confidentialite). 

En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978 et dans le respect du RGPD, l’utilisateur dispose d’un droit d’accès, de rectification, de suppression et d’opposition de ses données personnelles. L’utilisateur exerce ce droit par mail à [dpd@toobib.org](mailto:dpd@toobib.org).

**La finalité** de l’usage des données à caractère personnel collectées reste uniquement la finalité prévue au départ.

**La conservation** : les données sont conservées pendant toute la durée nécessaire aux finalités pour lesquelles elles ont été communiquées à InterHop, et archivées dans le respect de la réglementation applicable.

## Article 4 : Propriété intellectuelle

Les marques, logos, signes ainsi que tous les contenus du site (textes, images, son…) font l’objet d’une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d’auteur.

L’utilisateur doit solliciter l’autorisation préalable du site pour toute reproduction, publication, copie des différents contenus. Il s’engage à une utilisation des contenus du site dans un cadre strictement privé.

Toute représentation totale ou partielle du site [toobib.org](https://toobib.org) par quelque procédé que ce soit, sans l’autorisation expresse de l’exploitant du site Internet constituerait une contrefaçon sanctionnée par l’article L 335-2 et suivants du Code de la propriété intellectuelle.
Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’utilisateur qui reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source.

## Article 5 : Responsabilité

Les sources des informations diffusées sur le site [toobib.org](https://toobib.org) sont réputées fiables mais le site ne garantissent pas qu’il soit exempt de défauts, d’erreurs ou d’omissions.
Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle. Malgré des mises à jour régulières, le site [toobib.org](https://toobib.org)  ne peut être tenu responsable de la modification des dispositions administratives et juridiques survenant après la publication. De même, les sites ne peuvent être tenus responsables de l’utilisation et de l’interprétation de l’information contenue dans ce site.

L’utilisateur s’assure de garder précieusement son identifiant et son mot de passe secret. Toute divulgation de l’identifiant et du mot de passe, quelle que soit la forme, est interdite. 

L’utilisateur assume les risques liés à l’utilisation de son identifiant et mot de passe. Le site décline toute responsabilité.

Le site [toobib.org](https://toobib.org) ne peut pas être tenu pour responsable d’éventuels virus qui pourraient infecter l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou au téléchargement provenant de ce site.

La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d’un tiers.

Toobib s'engage à prévenir la fuite de données par tous les moyens de sécurité à sa disposition.

## Article 6 : Liens hypertextes

Des liens hypertextes peuvent être présents sur le site. L’utilisateur est informé qu’en cliquant sur ces liens, il sortira du site [toobib.org](https://toobib.org) . Ces derniers n’ont pas de contrôle sur les pages web sur lesquelles aboutissent ces liens et ne sauraient, en aucun cas, être responsables de leurs contenus.

## Article 7 : Cookies

Les services listés dans ces CGU n'utilisent pas de cookies.

## Article 8 : Droit applicable et juridiction compétente

La législation française s’applique au présent contrat. En cas d’absence de résolution amiable d’un litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître.

Toobib n'est pas éditeurs autres services listés dans ces CGU.

***Si votre projet sort du cadre défini par les CGU (projets recherche, services HDS par exemple), vous devez contacter la Déléguée à la Protection des données à [dpd@toobib.org](mailto:dpd@toobib.org). Ce type de projet nécessitera l'élaboration d'un contrat.***
