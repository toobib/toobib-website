---
title: "Maquette de toobib.org"
desc: "création de la maquette et du site"
image: "/images/blog-1.jpg"
date: 2023-09-12T05:00:00Z

---


## Toobib.org

[Toobib.org](https://toobib.org) est une plateforme web (site internet) sur laquelle les patient.e.s pourront chercher les professionnel.le.s de santé inscrit.e.s.

A partir de ce point d'accès central les professionnel.le.s de santé PS pourront s'inscrire aux services numériques disponibles (visio, dossier patient...) sur un des serveurs certifiés pour la santé (HDS) du réseau. Ceci participe à la décentralisation du stockage des données de santé et à l'autonomie des acteurs du réseau de soin.

Toobib.org est donc un équivalent dédié à la santé du site web [entraide.chatons.org](http://entraide.chatons.org).


## Porteur du projet

Ce projet est porté par l'association éponyme : [Toobib.org](https://toobib.org). 
Voici [un lien vers les membres](https://toobib.org/contact) de cette belle asssociation.

## Maquette

Avec cette maquette nous souhaitons vous montrer la future version du site web Toobib.org.
Voici l'url pour y avoir accès : [toobib.org/maquette](https://toobib.org/maquette).

![](https://pad.interhop.org/uploads/f471f8c7-9d3c-4302-b08e-b8bdc48e7070.png)

Merci particulièrement à Cyril VERNEUIL et Quentin PARROT pour leur travail réalisé cet été.

Le code du site web sera opensource en licence copy-left[^git].

Ce site présentera 
- l'association Toobib.
- les fonctionnalités du site internet Toobib.org permettant 
  - coté patient.e : la recherche de professionnel.le.s de santé 
  - coté soignant.e : l'inscription des professionnel.le.s de santé à la plateforme.

Nous souhaitons utiliser la police de caractère luciole[^luciole], spécifiquement conçu pour les malvoyant.e.s

## Communauté

Soignant.e.s ou patient.e.s nous serions ravis d'avoir vos retours.
Vous pouvez écrire à l'équipe de Toobib à contact@toobib.org ou discuter sur le forum matrix dédié à Toobib : [https://matrix.to/#/#toobib:matrix.interhop.org](https://matrix.to/#/#toobib:matrix.interhop.org).

[^luciole]: [https://www.luciole-vision.com/](https://www.luciole-vision.com/)

[^git]: [framagit.org/interhop/toobib](https://framagit.org/toobib/)


