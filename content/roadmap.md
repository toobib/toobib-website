---
title: Feuille de route
layout: roadmap
draft: false
mission: 
  - title: Mission
    answer: Toobib est d’abord un annuaire libre et open-source des professionnels de santé. Il permettra la mise en relation entre les praticiens et les patients dans un environnement libre et open source. Ce projet n’utilise que des technologies opensources!

objs:
  - title: Objectifs
    answer: 
      - Respecter la vie privée et des données personnelles des utilisateurs.trices
      - Consulter l’annuaire des professionnel.le.s de santé référencés sur Toobib
      - Vérifier et corriger les données stockées chez [annuaire.sante.fr](https://annuaire.sante.fr)
      - Créer et réserver des plages de rendez-vous

par:	
  title: Planning de réalisation
  goals:
    - year: 2022
      items:
        - Faire l'état de l'art des logiciels de prises de rendez-vous opensources existants.
        - Commencer à utiliser et adapter un logiciel opensource de prise de rendez-vous
    - year: 2023
      items:
        - Création de l'association Toobib
        - Découverte de l'API Pro Santé Connect
        - Découverte de l'API FHIR annuaire de santé
        - Création d'un [dossier patient associatif et opensource ToobibPro](https://demo.toobib.org)
    - year: 2024
      items:
        - Déploiement d'un gestionnaire d'authentification HDS (ToobibAuthentification)
        - Authentification via la e-CPS
        - Mise en place d'une messagerie instantanée et fédérée HDS (ToobibChat)
        - Mise en place d'un drive HDS (ToobibDrive)
        - Mise en place d'un gestionnaire de mots de passe HDS (ToobibSecret)
        - Création d’un formulaire de recherche permettant de questionner la base de données des professionnel.le.s de santé et d’afficher les résultats correspondants sur [Toobib.org](https://toobib.org/)
    - year: 2025
      items:
        - Début des travaux de certification du Logiciel d'Aide a la Prescription (LAP)
        - Création d’une page professionnelle permettant d’indiquer en détail les informations du praticien
        - Mise en production d'outils de visio-conférence dédiés à la santé HDS (ToobibConf)
        - Déploiement d'une boite d'e-mail interfacée avec Mailiz
---
