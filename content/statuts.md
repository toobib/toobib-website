---
title: Statuts
layout: statuts
draft: false

---


TOOBIB<br />ASSOCIATION REGIE PAR LA LOI DU 1er JUILLET 1901<br />SIEGE : Chez Céline Decultot, 7 bis allée Neuve, 54520 LAXO

# PREAMBULE

L’association entend oeuvrer pour des communs en santé et déployer une utilisation des données de santé éthique et transparente, respectueuse du droit à la protection des données personnelles dont les données couvertes par le secret médical sont une composante particulièrement sensible.<br />Toobib fait de la sensibilisation et de la formation auprès des professionnels de santé sur les enjeux du traitement des données de santé.<br />Toobib.org promeut, développe et met à disposition des outils, documents, guides, logiciels libres et open-sources par et pour les professionnel.le.s de santé.<br />Toobib.org regroupe notamment des professionnel.le.s de santé.

# ARTICLE 1 – DENOMINATION

Il est constitué entre les soussigné.e.s, ainsi que les personnes physiques ou morales qui adhéreront par la suite aux présents statuts, une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour dénomination : Toobib.

# ARTICLE 2 – OBJET

Cette association a pour objet :
      - la sensibilisation, la formation, ou le déploiement d'initiatives d'éducation populaire vers les professionnel.le.s de santé concernant le traitement et la protection des données de sant
      - la mise à disposition aux membres d'informations, guides, documents, outils numériques opensources promus, développés ou utilisés par l'association
      - la protection des données de santé hébergé
      - Elle a vocation à intégrer l’ensemble des profesionnel.le.s de santé désireux de s’impliquer dans un numérique éthique et protecteur des données de san
# ARTICLE 3 - SIEGE
 
Le siège de l’association est fixé chez Céline Decultot, 7 bis Allée Neuve 54520 LAXOU. Il pourra être transféré par simple décision du Conseil d’Administration.
  
# Article 4 – DUREE

La durée de l’association est illimitée.

# ARTICLE 5 – COMPOSITION ET ADMISSION DES MEMBRES
- L’association se compose de membres, personnes physiques et morales, se répartissant en deux catégories, à savoir :
    - Les membres adhérents sont admis conformément au règlement intérieur.
    - Membres d'honneur :Personnes physiques ou morales, ces membres peuvent acquérir la qualité de membre d’honneur sur proposition du Conseil d’Administration à la majorité des 2/3 et après ratification par l’Assemblée Générale à la majorité simple, en raison de services rendus à l’association."
        - Ils sont dispensés de cotisation.
        - Ils ont une voix délibérative lors de l'Assemblée Générale.
        - Ils peuvent être démis de leur statut de membre d’honneur à tout moment, à leur demande ou sur décision à la majorité simple du Conseil d’Administration
      - Membres adhérents(Les membres adhérents sont admis conformément au règlement intérieur.) :
      - Membres personnes morales: l’adhésion de personnes morales doit être approuvée par la majorité des 2/3 du Conseil d’Administration et nécessite le versement d’une cotisation annuelle dont le montant est fixé par le Conseil d’Administration.<br />Elles sont représentées par leurs représentants légaux ou les personnées déléguées à cet effet. Ils ont voix délibérative lors de l’Assemblée Générale.
      - Membres personnes physiques: il s’agit des personnes physiques s’étant acquittées du montant de la cotisation annuelle fixée par le Conseil d’Administration, notamment des professionnels de santé intéressés par les valeurs et l’objet de l’association.Ils ont voix délibérative lors de l’Assemblée Générale.

# ARTICLE 6 - RADIATIONS et SUSPENSIONS
- La qualité de membre se perd par :
  - Pour les mêmes motifs que pour la radiation, la qualité de membre peut être suspendue temporairement sur décision du Conseil d’Administration notamment dès lors qu’est envisagée une procédure de radiation, conformément au point 3 de cet article.
   - La démission notifiée au Conseil d’Administration ;"
   - Le décès ;"
- La radiation prononcée par le Conseil d’Administration pour non-versement répété de la cotisation annuelle, ou pour manquement grave aux valeurs de l’association, pour non-respect de ses statuts ou de son règlement intérieur, ou pour comportement incompatible avec le fonctionnement de l’association. L’intéressé peut demander dans un délai de 15 jours à compter de de la notification de la décision par le Conseil d’Administration, présenter des explications orales devant le bureau et/ou par écrit.


# ARTICLE 7 – AFFILIATION

L’association peut adhérer à d’autres associations, unions ou regroupements par décision du Conseil d’Administration.

# ARTICLE 8 – RESSOURCES

Les ressources de l’association sont constituées par:

Le Conseil d’Administration pourra refuser l’une de ces ressources dès lors que ce dernier jugerait qu’elles pourraient mettre en péril l’indépendance de l’association ou ses valeurs, ou encore si elles dépassent un montant qu’il juge trop important."

  - les cotisations des membres,
  - les financements participatifs (crowdfunding) ou produits de collectes,
  - les dons,
  - les subventions autorisées par la loi (subventions publiques, subventions de fondations oeuvrant dans l’intérêt général)
  - les recettes provenant de la vente de publications, créations ou prestations de services fournis conformément à son objet
  - les legs
  - toutes ressources autres autorisées par la loi ou le règlement

# title: ARTICLE 9 – MODALITES DE TENUE A DISTANCE DES INSTANCES ET MODALITES DE VOTES

La tenue des différentes instances de l’association (Assemblées générales et extraordinaires, Conseil d’Administration, Bureau) peut être réalisée à distance par voie de visioconférence et /ou conférence téléphonique.<br />Les votes des personnes assistant à distance aux instances sont comptabilisés de la même manière que les votes des personnes en présentiel.<br />A l’exception du Bureau, chacun des membres de chaque instance peut se faire représenter par un membre de la même instance. A cette occasion, il peut lui déléguer son vote par décision écrite.

# ARTICLE 10 - ASSEMBLEE GENERALE ORDINAIRE

L'Assemblée générale ordinaire comprend les membres d'honneur et tous les membres à jour de leurs cotisations le jour de sa tenue et ayant voix délibérative. Peuvent être invitées sans droit de vote, toutes les personnes dont le Conseil d'Administration estime la présence utile.<br />**Elle se réunit une fois par an.**<br />Quinze jours au moins avant la date fixée, les membres de l'association sont convoqués par les soins du.de la secrétaire par voie numérique (courriel).<br />L'ordre du jour est fixé par le Conseil d'Administration.<br />Il figure sur les convocations. <br />Le.la présidente, assisté des membres du conseil, préside l'assemblée et expose la situation de l'association. <br />Le.la trésorier.e rend compte de sa gestion et soumet les comptes annuels (bilan, compte de résultat et annexe) à l'approbation de l'assemblée. <br />Le.la secrétaire rédige le procès-verbal.<br />L'Assemblée générale approuve le bilan des activités de l'association et son rapport financier.<br />Les orientations proposées par le Conseil d'Administration sont présentées et approuvées à cette occasion.<br />L'Assemblée générale élit les membres entrants du Conseil d'Administration.<br />Ne peuvent être abordés que les points régulièrement inscrits à l'ordre du jour. Un vote peut être proposé par le Conseil d'Administration sur les résolutions.<br />Tout membre peut demander à voir un point spécifique inscrit à l'ordre du jour jusque 5 jours avant la tenue de l'Assemblée générale. Le Conseil d'Administration statue sur sa demande.<br />Les décisions sont prises à la majorité simple des voix des membres présents, et sous réserve que la moitié des membres du Conseil d'Administration soient présents.<br />Toutes les délibérations sont prises à main levée et lecture est faite des votes par correspondance ou à distance conformément à l'article 10 des présents statuts.

# ARTICLE 11 - ASSEMBLEE GENERALE EXTRAORDINAIRE

Sur proposition d'au moins la moitié des membres du Conseil d’Administration, une Assemblée générale extraordinaire peut être convoquée pour modifier les statuts ou la charte, prononcer la dissolution de l'association ou lorsque les intérêts de l'association l'exigent.<br />Les modalités de convocation et de tenue de l'assemblée sont les mêmes que pour l’Assemblée générale ordinaire. <br />Les délibérations sont prises à la majorité des deux tiers des membres présents et représentés, sous réserve d'obtenir le quorum de votes de la moitié des membres.<br />Si le quorum des deux tiers des membres présents et représentés n'était pas atteint, le Conseil d'Administration peut convoquer une seconde Assemblée Générale dans un délai de trois mois, qui pourra statuer par vote à la majorité simple.

# ARTICLE 12 - CONSEIL D’ADMINISTRATION

L'association est dirigée par un Conseil d’Administration de 3 à 20 personnes pouvant être des membres personnes physiques ou morales, à l'exception des personnes morales à but lucratif qui ne peuvent être admises dans cette instance.<br />Les administrateurs sont mandatés pour trois années, de manière illimitée. Le Conseil d'Administration est renouvelé par tiers tous les ans.<br />Sur convocation du.de la Président.e, le Conseil d'Administration se réunit au moins 2 fois par an et chaque fois que ses membres l'estiment nécessaire.<br />Il fonctionne de manière collégiale et les missions sont réparties entre ses membres.<br />Pour œuvrer à une représentation paritaire femme/homme, l’association encourage et met en oeuvre des conditions favorables à ce que les membres des instances dirigeantes soient représentés à part équivalente.<br />Toobib étant une association interprofessionnelle, l’association encourage et met en oeuvre des conditions favorables à la participation de membres représentant l'ensemble des professionnels de santé adhérents à l'association aux instances dirigeantes. Les professions de santé sont celles définies dans le code de la santé publique[^csp_ps].<br /> [^csp_ps]: https://www.vie-publique.fr/fiches/37855-qui-sont-les-professionnels-de-sante-code-se-la-sante-publique <br />Les décisions sont prises à la majorité simple et à main levée.<br />Sauf:<br />- la validation des candidatures des personnes morales souhaitant devenir membre de l'association ;<br />- la proposition des membres d'honneur ;<br />- les décisions de radiation ou suspension de membres ;<br />- la proposition de modification des statuts, de la charte et du règlement intérieur. <br />En pareil cas, la décision doit être prise à la majorité des 2/3 des votes.<br />Le Conseil d'Administration est compétent pour prendre toutes décisions relatives à l'association et notamment en matière de finances, projets et choix stratégiques de l'association.<br />Il établit l'ordre du jour des Assemblées Générales Ordinaires et Extraordinaires.<br />En cas de vacance exceptionnelle d'un membre, le Conseil d'Administration peut pourvoir lui-même au remplacement du membre jusqu'à ratification du remplacement par la prochaine Assemblée Générale Ordinaire. Une fois approuvé lors de l'Assemblée Générale Ordinaire, il assure la mission d'administrateur jusqu'à la fin du mandat initialement porté par son prédecesseur.

# ARTICLE 13 – LE BUREAU
  - Chaque année, le Conseil d’Administration élit en son sein, un bureau composé a minima de :" 
  - Le bureau est élu pour 1 an.<br />Le bureau est chargé de la mise en oeuvre opérationnelle des activités de l’association.<br />L’association est représentée en justice par son Président ou toute personne du Conseil d’Administration mandaté par ce dernier, et peut ester en justice selon ces mêmes-conditions.<br />Le.la secrétaire est chargé de tout ce qui concerne la correspondance et les archives. Il rédige les procès-verbaux des délibérations du conseil et en assure la transcription sur les registres. En cas d'absence ou de maladie, il est remplacé par un membre du conseil désigné par le président qui dispose alors des mêmes pouvoirs.<br />Le.la trésorier.e dresse le rapport financier de l’association. Il assure de la régularité et la sincérité des comptes. En cas d'absence ou de maladie, il est remplacé par un membre du conseil désigné par le président qui dispose alors des mêmes pouvoirs.<br />Les membres sortants sont rééligibles."
  - Un.e président.e ; 
  - Un.e secrétaire général.e ; 
  - Un.e trésorier.e.

# ARTICLE 14 – CHARTRE

Une charte des valeurs de l'association est rédigée à la création de l'association par les membres d'honneur. Elle peut être modifiée sur proposition des 2/3 du Conseil d'Administration et votée en Assemblée Générale extraordinaire.<br />Elle a pour objet de rappeler les principes essentiels portés par l’association et les valeurs que celle-ci entend défendre dans le cadre de ses activités.<br />Le non-respect de ces valeurs peut être un motif d’exclusion des membres.

# ARTICLE 15 – RÉGLEMENT INTÉRIEUR

Un règlement intérieur est établi par le Conseil d’Administration.<br />Il peut être modifié sur proposition des 2/3 du Conseil d’Administration et voté en Assemblée Générale extraordinaire.
<br />Il comprend notamment et de façon non exhaustive :
      - les modalités d’adhésion et les modalités d’exclusion des membres.
      - les modalités de fonctionnement des instances.

# ARTICLE 16 – PREVENTION DES CONFLITS D’INTERET

Chaque membre du Conseil d’Administration produit une déclaration annuelle d’intérêts à destination de l’association. Cette DPI est rédigée conformément au règlement intérieur.

Le Conseil d’Administration peut solliciter à tout moment que le membre pour lequel il existe un risque de conflit d’intérêt sur un point soulevé au débat soit exclu de la délibération, et s’abstienne de participer à toute réunion, discussions ou travaux préparatoires.

Il est attendu que le membre anticipe et veille de lui-même à se déporter.

# ARTICLE 17 – DISSOLUTION

Sur proposition du Conseil d’Administration, l’association peut être dissoute par l’Assemblée Générale Extraordinaire selon les modalités prévues à l’article 11.<br />Un ou plusieurs liquidateurs sont nommés lors de l’Assemblée Générale Extraordinaire.<br />L’actif, s’il y a lieu, ne peut être en aucun cas dévolu à ses membres. Il doit faire l’objet d’une dévolution à des associations de même nature.