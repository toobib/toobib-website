---
title: "Mentions légales"
desc: ""
---


# Code entreprises

* SIREN : 924 322 191
* SIRET : 924 322 191 0017
* NAF (Nomenclature d’Activités Française) : 72.11Z
* RNA (Répertoire National des Association) : W543016140
* Numéro TVA IC : Non-assujetti


# Nous retrouver

* Siège social : Chez Céline Decultot, 7 bis allée Neuve, 54520 LAXOU
* Site Web : [toobib.org](https://toobib.org)
* Courriel : [contact@toobib.org](mailto:contact@toobib.org)

# Equipe

- Responsable de traitement et directeur de la publication : Céline Decultot, Président de l’Association Toobib
- Déléguée à la protection des données (DPD): Chantal CHARLOT @: dpd@toobib.org

# Editeur

* **L'association InterHop édite** 
  - le site web [Toobib.org](https://toobib.org)
  - le logiciel de gestion de patientèle [ToobibPro](demo.toobib.org) dont une version de démo est en ligne
Ces deux logiciels sont distribués sous licence GNU Affero General Public License (AGPL)

* **Toobib Drive utilise CryptPad, éditeur collaboratif** à code source ouvert et sécurisé, qui permet d'utiliser les principaux modules de CryptPad (CryptDrive, Rich text, Code, Presentation, Poll, Kanban, Whiteboard, et Todo and Sheet). Cryptpad est sous licence GNU Affero General Public License (AGPL)

* **Toobib Secret utilise Bitwarden et Vaultwarden** qui est sous licence GNU Affero General Public License (AGPL).

* **Toobib Chat utilise Element** maintenu par la société New Vector Limited édite. Element est distribué sous la licence Apache 2.0.

* **Toobib Authentificaition utilise Keycloak**. Ce projet communautaire est sous la tutelle de Red Hat qui l'utilise comme projet en amont pour sa version Red Hat de Keycloak.  Keycloak est distribué sous la licence Apache 2.0.

# Hébergeur des sites

Toobib administre des services nécessitant un Hébergeur de Données de Santé (ci-après HDS) et des services qui ne le nécessitent pas.

En ce qui concerne les services HDS, il s’agit de : 
- [Toobib Drive](https://drive.toobib.org)
- [Toobib Chat](https://chat.toobib.org) 
- [Toobib Secret](https://secret.toobib.org) 
- [Toobib Authentification](https://auth.toobib.org/)

Les données à caractère personnel (ci-après données personnelles) traitées en utilisant les services HDS sont hébergés en France par l’HDS certifié [GPLExpert](https://gplexpert.com/). 


En ce qui concerne les services qui ne sont pas HDS, il s’agit de : 
- [Toobib](https://toobib.org )
- [Toobib Pro](https://demo.toobib.org) : version Demo



Les données personnelles traitées en utilisant les services ne nécessitant pas un HDS sont hébergés en France. 
Les sites web de [toobib.org](https://toobib.org) et la version démo de [Toobib Pro](https://demo.toobib.org) sont hébergés en France chez OVH (VPS).

# Cookies

Toobib n’utilisent pas de cookies.
En ce qui concerne les cotisations (page : [toobib.org/cotisations](https://toobib.org/cotisations) notamment) Toobib utilise les widgets du site [HelloAsso.com](https://www.helloasso.com/) qui contient des traceurs “Google Tag Manager”.

Lorsqu'un service opensource tierce est délivré par Toobib les cookies non essentiels sont systématiquement désactivés.

# Informations personnelles

Conformément aux dispositions de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, et au Règlement Général sur la Protection des données, aucune information personnelle n’est collectée à votre insu ou cédée à des tiers. Pour exercer vos droits ou éclaircir une information intégrée à la politique de confidentialité d’InterHop, vous pouvez contacter la Déléguée à la Protection des Données (DPD). Pour cela, il vous suffit d’envoyer un courriel à [dpd@toobib.org](mailto:dpd@toobib.org) ou un courrier par voie postale à Association Toobib, Chez Céline Decultot, 7 bis allée Neuve, 54520 LAXOU, en justifiant de votre identité.

# Utilisation d’images et d’icônes
* Icônes : flaticon.com, iconfinder.com
* Logo Toobib : Merci à Quentin PARROT
* Design Toobib : Merci à Cyril Verneuil
* Police de caractères [Luciole](https://www.luciole-vision.com/)


# Thèmes React utilisés

Merci à Gethugothemes et à l'équipe de Toobib

Vous retrouvez la politique de confidentialité d'InterHop [ici](https://toobib.org/politique-confidentialite/)


