#!/bin/zsh

export PATH=/root/.nvm/versions/node/v20.5.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/nvim-linux64/bin

SESSION_NAME="toobib-website-dev"
PROJECT_DIR="/home/git/toobib-website-dev"
FILE_TO_OPEN="README.md"

# Start a new tmux session
tmux new-session -d -s "$SESSION_NAME" -c "$PROJECT_DIR"

# Rename first window
tmux rename-window -t "$SESSION_NAME" "DevOps"

# Run the build and start command in the first pane
tmux send-keys -t "$SESSION_NAME":1 "npm run start" C-m

# Split the first window into two panes
tmux split-window -h -t "$SESSION_NAME" -c "$PROJECT_DIR"

# Create a second window for editing
tmux new-window -t "$SESSION_NAME" -n "Editor" -c "$PROJECT_DIR"

# Open nvim with FILE_TO_OPEN
tmux send-keys -t "$SESSION_NAME":2 "nvim $FILE_TO_OPEN" C-m

# Attach to the session
tmux attach-session -t "$SESSION_NAME"
