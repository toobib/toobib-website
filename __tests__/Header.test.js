// Header.test.js
import React from "react";
import { render, screen } from "@testing-library/react";
import { useRouter } from "next/router";
import { SessionProvider, useSession } from "next-auth/react";
import Header from "@layouts/partials/Header"; // Adjust the import path as needed
import "@testing-library/jest-dom";
import menu from "../config/menu.json";

jest.mock("../config/menu.json", () => ({
  main: [
    {
      name: "Outils",
      url: "/outils",
    },
    {
      name: "Cotisations",
      url: "/cotisations",
    },
    {
      name: "Blog",
      url: "/blogs",
    },
    {
      name: "Mon profil",
      url: "/mon-profil",
    },
    {
      name: "Trouver son Toobib",
      url: "/rechercher-un-professionnel-de-sante",
    },
  ],
  footer: [
    {
      name: "L'Association",
      menu: [
        {
          text: "Statuts",
          url: "/statuts",
        },
        {
          text: "Qui sommes-nous ?",
          url: "/qui-sommes-nous",
        },
        {
          text: "Nous contacter",
          url: "/contact",
        },
      ],
    },
  ],
}));

// Mock the useRouter hook
const mockPush = jest.fn();
jest.mock("next/router", () => ({
  useRouter: jest.fn(),
}));

// Mock the useSession hook from next-auth/react
jest.mock("next-auth/react", () => ({
  useSession: jest.fn(),
  signIn: jest.fn(),
  signOut: jest.fn(),
  SessionProvider: ({ children }) => <div>{children}</div>,
}));

describe("Header component", () => {
  beforeEach(() => {
    useRouter.mockReturnValue({
      push: jest.fn(),
      asPath: "/",
    });

    // Mocking session
    useSession.mockReturnValue({
      data: null,
      status: "unauthenticated",
    });
  });

  it("should show the mocked menu items", () => {
    render(
      <SessionProvider>
        <Header />
      </SessionProvider>,
    );
    expect(screen.getByText("Outils")).toBeInTheDocument();
    expect(screen.getByText("Cotisations")).toBeInTheDocument();
    expect(screen.getByText("Blog")).toBeInTheDocument();
    expect(screen.getByText("Trouver son Toobib")).toBeInTheDocument();
  });

  it("should show the menu items from menu.json", () => {
    render(
      <SessionProvider>
        <Header />
      </SessionProvider>,
    );

    menu.main.forEach((item) => {
      if (item.name !== "Mon profil") {
        const linkElement = screen.getByRole("link", { name: item.name });
        expect(linkElement).toBeInTheDocument();
        expect(linkElement).toHaveAttribute("href", item.url);
      }
    });
  });
  it('should show "Mon profil" when user is authenticated', () => {
    useSession.mockReturnValue({
      data: { user: { name: "Test User" } },
      status: "authenticated",
    });

    render(
      <SessionProvider>
        <Header />
      </SessionProvider>,
    );

    // Check if "Mon profil" is present in the document
    expect(screen.getByText("Mon profil")).toBeInTheDocument();
  });
  it('should not show "Mon profil" when user is not authenticated', () => {
    useSession.mockReturnValue({
      data: null,
      status: "unauthenticated",
    });

    render(
      <SessionProvider>
        <Header />
      </SessionProvider>,
    );

    // Check if "Mon profil" is not present in the document
    expect(screen.queryByText("Mon profil")).not.toBeInTheDocument();
  });
});
