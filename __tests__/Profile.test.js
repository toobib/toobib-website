import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import Profile from "@layouts/Profile";

jest.mock("next-auth/react");
import { useSession } from "next-auth/react";

jest.mock("../lib/api/Keycloak/AccountApi/getAccount.js");
import getAccount from "@lib/api/Keycloak/AccountApi/getAccount";


// Mock router to avoir unmounted next router
jest.mock("next/router", () => ({
  useRouter: jest.fn()
}));
import { useRouter } from "next/router";
useRouter.mockReturnValue({
  query: {},
  push: jest.fn(),
});

describe("Profile Component", () => {
  const mockSession = {
    data: {
      accessToken: "mockAccessToken",
    },
  };

  const mockAccount = {
    firstName: "test",
    lastName: "test",
    email: "mail@example.com",
    attributes: {
      rppsId: "123456",
      Phone: "123-456-7890",
      PractionerRole: "Medicin",
      Cabinet: JSON.stringify({
        1: { location: "Location 1", phone: "123-456-7890" },
        2: { location: "Location 2", phone: "987-654-3210" },
      }),
    },
  };

  beforeEach(() => {
    useSession.mockReturnValue(mockSession);
    getAccount.mockResolvedValue(mockAccount);
  });

  it("renders multiple CabinetDisplay components when multiple cabinets are present", async () => {
    render(<Profile data={{}} />);

    await waitFor(() => {
      expect(screen.getByText("Cabinet 1")).toBeInTheDocument();
      expect(screen.getByText("Cabinet 2")).toBeInTheDocument();
    });
  });

  it("displays correct profile information", async () => {
    render(<Profile data={{}} />);

    await waitFor(() => {
      expect(screen.getByText("test test")).toBeInTheDocument();
      expect(screen.getByText("Numéro RPPS : 123456")).toBeInTheDocument();
      expect(screen.getByText("Numéro de téléphone : 123-456-7890")).toBeInTheDocument();
      expect(screen.getByText("Adresse e-mail : mail@example.com")).toBeInTheDocument();
    });
  });
});
