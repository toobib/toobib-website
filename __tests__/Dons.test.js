import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Cotisations from "../layouts/Cotisations"; // Adjust the import path as needed

describe("Cotisations component", () => {
  const fakeData = {
    frontmatter: {
      title: "Test Title",
      articles: [
        {
          title: "Test Title Article",
          descriptions: [{ text: "Content article" }],
        },
        {
          title: "Test Title Article2",
          descriptions: [{ text: "Content article2" }],
        },
        {
          title: "Test Title Article3",
          descriptions: [{ text: "Content article3" }],
        },
        {
          title: "Via Hello Asso",
          descriptions: [{ text: "helloasso" }],
        },
      ],
    },
  };

  it("Renders without crashing", () => {
    render(<Cotisations data={fakeData} />);
    expect(
      screen.getByRole("heading", { name: "Test Title" }),
    ).toBeInTheDocument();
  });

  it("Renders multiple articles and helloasso iframe", () => {
    render(<Cotisations data={fakeData} />);
    fakeData.frontmatter.articles.forEach((article) => {
      expect(
        screen.getByRole("heading", { name: article.title }),
      ).toBeInTheDocument();
      article.descriptions.forEach((description) => {
        if (description.text === "helloasso") {
          const iframe = screen.getByTestId("helloasso-iframe");
          expect(iframe).toBeInTheDocument();
          expect(iframe).toHaveAttribute(
            "src",
            "https://www.helloasso.com/associations/toobib/adhesions/cotisation-toobib/widget",
          );
        } else {
          expect(screen.getByText(description.text)).toBeInTheDocument();
        }
      });
    });
  });
});
