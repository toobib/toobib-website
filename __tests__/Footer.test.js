import { render, screen } from "@testing-library/react";
import Footer from "@layouts/partials/Footer"; // Adjust the import path as necessary
import config from "../config/config.json";
import "@testing-library/jest-dom";
import menu from "../config/menu.json";

describe("Footer", () => {
  it("renders all footer links with correct URLs", () => {
    render(<Footer />);

    // Check each menu link
    menu.footer.forEach((col) => {
      const colElement = screen.getByText(col.name);
      expect(colElement).toBeInTheDocument();
      col.menu.forEach((item) => {
        const linkElement = screen.getByText(item.text);
        expect(linkElement).toBeInTheDocument();
        expect(linkElement.closest("a")).toHaveAttribute("href", item.url);
      });
    });
  });
});
