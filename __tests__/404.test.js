import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import NotFound from "@layouts/404";

describe("NotFound", () => {
  const fakeData = {
    frontmatter: { title: "Test Title" },
    content: "Test Content",
  };

  it("renders without crashing", () => {
    render(<NotFound data={fakeData} />);
    expect(
      screen.getByRole("heading", { name: "Test Title" }),
    ).toBeInTheDocument();
  });
  it("renders content correctly", () => {
    render(<NotFound data={fakeData} />);
    expect(screen.getByText("Test Content")).toBeInTheDocument();
  });
  it("converts markdown content correctly", () => {
    const fakeDataMd = {
      ...fakeData,
      content: "**Test** Content",
    };
    render(<NotFound data={fakeDataMd} />);
    expect(screen.getByText("Test").closest("strong")).toBeInTheDocument();
    expect(screen.getByText("Content")).toBeInTheDocument();
  });
});
