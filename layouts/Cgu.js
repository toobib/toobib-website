import Link from "next/link";
import Cta from "./components/Cta";
import { markdownify } from "@lib/utils/textConverter";

function Cgu({ data }) {
  const {
    frontmatter: { title, intro, articles },
  } = data;
  return (
    <>
      <section className="section">
        <div className="container utilities-body">
          <h1 className="text-center font-normal">{title}</h1>
          <div className="section p-12 shadow">
            {markdownify(intro[0].descriptions[0].text, "p")}
          </div>
          <div className="section p-12 shadow mt-6">
            {articles.map((article, index) => (
		<div className="py-6">
		{markdownify(article.title, "h3")}
		{markdownify(article.description, "p")}
		{article.rights && article.rights.map((right, index) => (
			<div className="py-3">
			{markdownify(right.title, "h4")}	
			{markdownify(right.description, "p")}	
			</div>
		))}
		</div>
	    ))}
          </div>
        </div>
      </section>
    </>
  );
}

export default Cgu;
