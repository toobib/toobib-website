// https://stackoverflow.com/questions/75835946/why-nextjs-link-is-prefetching-on-each-hover-how-to-disable-it
import router from 'next/router';

function CustomLink(props) {
  const linkHref = props.href;

  function customLinkOnClick(e) {
    e.preventDefault();
    router.push(linkHref);
  }

  return (
    <a href={linkHref} onClick={customLinkOnClick}>
      {props.children}
    </a>
  );
}

export default CustomLink;
