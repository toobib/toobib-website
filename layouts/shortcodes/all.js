import BannerWithCta from "./BannerWithCta";
import YoutubePlayer from "../components/YoutubePlayer";
import Button from "./Button";

const shortcodes = {
  BannerWithCta,
  Button,
  YoutubePlayer,
};

export default shortcodes;
