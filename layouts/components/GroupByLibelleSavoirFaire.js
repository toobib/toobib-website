
const groupByLibelleSavoirFaire = (array, codeRefKey, labelKey) => {
  return array.reduce((result, currentValue) => {
    if (!currentValue[codeRefKey] || !currentValue[labelKey]) {
      return result;
    }
    const keyValue = currentValue[codeRefKey];
    const labelValue = currentValue[labelKey];
    if (!result[keyValue]) {
      result[keyValue] = { label: labelValue, specialties: [] };
    }
    result[keyValue].specialties.push(currentValue);
    return result;
  }, {});
};

export default groupByLibelleSavoirFaire;
