import { Map, Marker } from "pigeon-maps";
const AdresseMap = ({ latitude, longitude }) => {
  return (
    <Map height={250} defaultCenter={[latitude, longitude]} defaultZoom={11}>
      <Marker anchor={[latitude, longitude]} />
    </Map>
  );
};
export default AdresseMap;
