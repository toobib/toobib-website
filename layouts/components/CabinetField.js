import React from "react";

const CabinetField = ({ id, formik }) => {
  const handleDeleteClick = () => {
    const newCabinet = { ...formik.values.Cabinet };
    delete newCabinet[id];
    formik.setFieldValue("Cabinet", newCabinet);
  };
  return (
    <>
      <div className="col-span-2 text-center flex items-center justify-center mb-10 mt-10 space-x-4">
        <p className="text-sky-800 text-xl font-bold">
          Lieu de consultation {id}
        </p>
        <button
          type="button"
          className="flex items-center text-sky-800 text-sm space-x-2"
          onClick={handleDeleteClick}
        >
          <svg
            data-slot="icon"
            fill="none"
            strokeWidth="2"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            className="w-6 h-6"
          >
            <title>
              Supprimer le lieu de consultation {id}
            </title>
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"
            />
          </svg>
        </button>
      </div>

      <div className="col-span-1 fadeIn">
        <label
          htmlFor={`Cabinet.${id}.Lieu`}
          className="block text-sm font-medium text-gray-700"
        >
          Lieu
        </label>
        <input
          id={`Cabinet.${id}.Lieu`}
          name={`Cabinet.${id}.Lieu`}
          type="text"
          required
          onChange={formik.handleChange}
          value={formik.values.Cabinet[id].Lieu}
          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        />
      </div>
      <div className="col-span-1 fadeIn">
        <label
          htmlFor="adresse"
          className="block text-sm font-medium text-gray-700"
        >
          Adresse
        </label>
        <input
          id={`Cabinet.${id}.Adresse`}
          name={`Cabinet.${id}.Adresse`}
          type="text"
          required
          onChange={formik.handleChange}
          value={formik.values.Cabinet[id].Adresse}
          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        />
      </div>
      <div className="col-span-1 fadeIn">
        <label
          htmlFor="CodePostal"
          className="block text-sm font-medium text-gray-700"
        >
          Code Postal
        </label>
        <input
          required
          id={`Cabinet.${id}.CodePostal`}
          name={`Cabinet.${id}.CodePostal`}
          type="text"
          onChange={formik.handleChange}
          value={formik.values.Cabinet[id].CodePostal}
          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        />
      </div>
      <div className="col-span-1 fadeIn">
        <label
          htmlFor="Ville"
          className="block text-sm font-medium text-gray-700"
        >
          Ville
        </label>
        <input
          required
          id={`Cabinet.${id}.Ville`}
          name={`Cabinet.${id}.Ville`}
          type="text"
          onChange={formik.handleChange}
          value={formik.values.Cabinet[id].Ville}
          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        />
      </div>
      <p className="col-span-2 text-center text-sky-800 text-xl font-bold fadeIn">
        Informations complémentaires
      </p>
      <div className="col-span-1 fadeIn">
        <label
          htmlFor="phone-lieu"
          className="block text-sm font-medium text-gray-700"
        >
          Téléphone du lieu de consultation
        </label>
        <input
          required
          id={`Cabinet.${id}.PhoneLieuConsultation`}
          name={`Cabinet.${id}.PhoneLieuConsultation`}
          type="text"
          onChange={formik.handleChange}
          value={formik.values.Cabinet[id].PhoneLieuConsultation}
          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        />
      </div>
      <div className="col-span-1 fadeIn">
        <label
          htmlFor="etage"
          className="block text-sm font-medium text-gray-700"
        >
          Étage
        </label>
        <input
          id={`Cabinet.${id}.Etage`}
          name={`Cabinet.${id}.Etage`}
          type="text"
          onChange={formik.handleChange}
          value={formik.values.Cabinet[id].Etage}
          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        />
      </div>
      <div className="col-span-1 fadeIn">
        <label
          htmlFor="ascenceur"
          className="block text-sm font-medium text-gray-700"
        >
          Ascenceur
        </label>
        <input
          id={`Cabinet.${id}.Ascenceur`}
          name={`Cabinet.${id}.Ascenceur`}
          type="text"
          onChange={formik.handleChange}
          value={formik.values.Cabinet[id].Ascenceur}
          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        />
      </div>
      <div className="col-span-1 fadeIn">
        <label
          htmlFor="AccesHandicapes"
          className="block text-sm font-medium text-gray-700"
        >
          Accès handicapés
        </label>
        <input
          id={`Cabinet.${id}.AccesHandicapes`}
          name={`Cabinet.${id}.AccesHandicapes`}
          type="text"
          onChange={formik.handleChange}
          value={formik.values.Cabinet[id].AccesHandicapes}
          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        />
      </div>
      <div className="col-span-1 fadeIn">
        <label
          htmlFor="autre"
          className="block text-sm font-medium text-gray-700"
        >
          Autre
        </label>
        <input
          id={`Cabinet.${id}.Autre`}
          name={`Cabinet.${id}.Autre`}
          type="text"
          onChange={formik.handleChange}
          value={formik.values.Cabinet[id].Autre}
          className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        />
      </div>
    </>
  );
};

export default CabinetField;
