
import React from "react";
import Logo from "@components/Logo";


function Loading({ data }) {
  return (
        <div className="flex flex-col p-5 items-center justify-center md:p-20 mb-10 md:mb-40 mt-10 md:mt-40 text-xl md:text-2xl font-extrabold">
          <div className="animate-bounce mb-10">
            <br/>
            <Logo src="/images/logo_toobib_small.png"/>
          </div>
          <div>Chargement en cours ...</div>
        </div>
);
}

export default Loading;
