import { Formik, Form, Field, ErrorMessage } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import uploadPhoto from "@lib/api/PhotoApi/uploadPhoto";
const validationSchema = Yup.object({
  photo: Yup.mixed().required("Une photo est nécessaire"),
});

const ChangePhotoForm = ({ id, onPhotoChange, token, onPhotoUploadError }) => {
  const [showButton, setShowButton] = useState(false);
  const [labelText, setLabelText] = useState("Téléchargement");
  return (
    <Formik
      initialValues={{ photo: null }}
      validationSchema={validationSchema}
      onSubmit={async (values, { setSubmitting, resetForm }) => {
        const originalFile = values.photo;
        const newFileName = `${id}${originalFile.name.substring(originalFile.name.lastIndexOf("."))}`;
        const newFile = new File([originalFile], newFileName, {
          type: originalFile.type,
        });

        const formData = new FormData();
        formData.append("file", newFile);
        const respose = await uploadPhoto(formData, token);
        if (respose.error) {
          onPhotoUploadError();
        }
        onPhotoChange();
        setSubmitting(false);
        resetForm();
        setShowButton(false);
        setLabelText("Téléchargement");
      }}
    >
      {({ setFieldValue, isSubmitting }) => (
        <Form className="max-w-lg mx-auto p-4 bg-white shadow-md rounded-lg">
          <div className="mb-4">
            <label
              htmlFor="photo"
              className="block text-sm font-medium text-gray-700"
            >
              Modifier la photo de profil
            </label>
            <input
              id="photo"
              name="photo"
              type="file"
              onChange={(event) => {
                setFieldValue("photo", event.currentTarget.files[0]);
                setShowButton(true);
                setLabelText(
                  event.currentTarget.files[0]
                    ? event.currentTarget.files[0].name
                    : "Téléchargement",
                );
              }}
              className="hidden"
            />
            <label
              htmlFor="photo"
              className="mt-1 block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer focus:outline-none focus:ring-2 focus:ring-blue-500 bg-white py-2 px-4 text-center"
            >
              {labelText}
            </label>
            <ErrorMessage
              name="photo"
              component="div"
              className="text-red-500 text-sm mt-1"
            />
          </div>
          {showButton && (
            <button
              type="submit"
              disabled={isSubmitting}
              className="btn btn-primary mb-5 z-0 py-[14px] md:w-full w-full flex items-center text-center justify-center"
            >
              {isSubmitting ? "Téléchargement..." : "Modifier"}
            </button>
          )}
        </Form>
      )}
    </Formik>
  );
};

export default ChangePhotoForm;
