import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Button from "./Button";

describe("Button", () => {
  it("navigates to the linked page", () => {
    render(
      <Button href="/test-link">
        Test Button
      </Button>,
    );
    const button = screen.getByText("Test Button");
    expect(button).toHaveAttribute("href", "/test-link");
  });
});
