import {signIn, useSession, signOut as appSignOut} from 'next-auth/react';
import Loading from '@components/Loading';
import React from 'react';
import LogOutButton from "@components/LogOutButton";


function LogInButton() {
  const { data: session, status } = useSession();

  if (status === 'loading') {
    return <Loading />;
  }

  if (!session) {
    return (
      <button className="btn btn-primary z-0 py-[14px]" onClick={() => signIn('keycloak')}>
        Se connecter
      </button>
    );
  }

  return (
    <LogOutButton>

    </LogOutButton>
  );
}

export default LogInButton;
