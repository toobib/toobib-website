import { useState, useEffect } from 'react';

const useWindowSize = () => {

  // Déclare la variable d'état windowSize avec ses propriétés width et height initialisées à undefined.
  // setWindowSize est le setter permettant de mettre à jour windowSize.
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });

  // Utilise useEffect pour exécuter le code au chargement de la page.
  useEffect(() => {
    // Déclare une fonction pour récupérer les dimensions actuelles de la fenêtre.
    const handleResize = () => {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };

    // Ajoute un écouteur d'événements à la fenêtre pour mettre à jour les dimensions lorsqu'elles changent.
    window.addEventListener('resize', handleResize);

    // Exécute handleResize pour définir les dimensions initiales.
    handleResize();

    // Nettoie l'écouteur d'événements lors du démontage du composant pour éviter les fuites de mémoire.
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []); // Le tableau de dépendances vide signifie que cet effet s'exécute une seule fois après le premier rendu.

  // Retourne l'objet windowSize contenant les dimensions actuelles de la fenêtre.
  return windowSize;
};

export default useWindowSize;
