import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import React, { useEffect, useState } from "react";
import { getProfessionsJSON, getSpecialitiesJSON } from "@lib/api/SearchApi/getProfessionsAndSpecialities";
import TextFieldWithDynamicPlaceholderToobib from "@components/TextFieldWithDynamicPlaceholderToobib";
import TextFieldWithDynamicPlaceholderAdresse from "@components/TextFieldWithDynamicPlaceholderAdresse";
import Loading from "@components/Loading";
import Papa from 'papaparse';
import groupByLibelleSavoirFaire from "@components/GroupByLibelleSavoirFaire";

const SearchPsForm = ({ onSubmit }) => {
  const [professionOptions, setProfessionOptions] = useState([]);
  const [errorFhir, setErrorFhir] = useState();
  const [loading, setLoading] = useState(true);
  const [professionsJson, setProfessionsJson] = useState({});
  const [specialitiesJson, setSpecialitiesJson] = useState({});
  useEffect(() => {
    const fetchProfessions = async () => {
      try {
        const professions = await getProfessionsJSON()
        setProfessionsJson(professions)
        const specialities = await getSpecialitiesJSON()
        setSpecialitiesJson(specialities)
      } catch (error) {
        setErrorFhir("Error fetching professions");
      } finally {
        setLoading(false);
      }
    };
    fetchProfessions();
  }, []);

  const handleProfessionOptionsSet = (results) => {
    if (results.length === 0 || results.length === undefined) {
      setErrorFhir("FHIR DOWN");
    }
  };

  const validationSchema = Yup.object()
    .shape({
      toobib: Yup.string(),
      adresse: Yup.string(),
      profession: Yup.string(),
      specialty: Yup.string(),
      rangeAround: Yup.string(),
    })
    .test(
      "at-least-two-fields",
      "Au moins deux champs doivent être remplis",
      function (values) {
        const { toobib, adresse, profession, rangeAround, position, specialty } = values;
        const filledFields = [toobib, adresse, profession, rangeAround].filter(Boolean).length
        return filledFields >= 2;
      },
    );

  if (loading) {
    return <Loading></Loading>;
  }
  return (
    <Formik
      initialValues={{ toobib: "", adresse: "", profession: undefined, rangeAround: undefined, position:undefined, specialty:undefined }}
      validationSchema={validationSchema}
      onSubmit={async (values, { setFieldValue, setSubmitting, setErrors }) => {
        setSubmitting(true);
        if (validationSchema.isValidSync(values)) {
          try {
            const results = await onSubmit(values);
          } finally {
            setSubmitting(false);
          }
        } else {
          setErrors({
            atLeastTwoFields:
              "Au moins deux champs doivent être remplis afin d'affiner votre recherche",
          });
          setSubmitting(false);
        }
      }}
    >
      {({ isSubmitting, errors, handleChange, values }) => (
        <section className="section">

          {errorFhir === "FHIR DOWN" ? (
            <div className="flex items-center justify-center mb-96">
              <div className="flex flex-col items-center justify-center">
                <h5 className="text-red-500 fadeIn mt-5 mb-5 p-4 bg-red-100 rounded-3xl text-center">
                  Nous utilisons l'API « Annuaire Santé » de l'ANS, qui semble
                  être indisponible, réessayez plus tard. Pour plus
                  d'informations&nbsp;
                  <a
                    href="https://status.annuaire-sante.esante.gouv.fr/"
                    className="text-blue-500 underline hover:text-blue-800"
                  >
                    status.annuaire-sante
                  </a>
                </h5>
              </div>
            </div>
          ) : (
            <div className="flex flex-col items-center justify-center">
              <h1 className="text-center font-normal mb-16">
                Rechercher un professionnel de santé
              </h1>

              <Form className="flex flex-col justify-center md:w-3/6 xl:w-4/12">
                <div>
                  <TextFieldWithDynamicPlaceholderToobib
                    id="toobib"
                    name="toobib"
                    placeholderText="Nom du Toobib"
                  />
                  <ErrorMessage name="toobib" component="div"/>
                </div>
                <div>
                  <Field
                    as="select"
                    id="profession"
                    name="profession"
                    className="hover:border-sky-800 mt-8 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm text-center"
                    onChange={handleChange}
                    value={values.profession}
                  >
                    <option value="">Sélectionnez une profession</option>
                      {professionsJson &&
  Object.entries(professionsJson).map(([key, value]) => (
    <option key={value} value={value}>
      {key}
    </option>
  ))}
                  </Field>
                  <ErrorMessage name="profession" component="div"/>
                  {values.profession === "medecin" ? (
                    <div className="fadeIn mt-5">
                      <p className="text-center text-red-500">
                        Veuillez sélectionner la spécialité du médecin
                      </p>
                      <Field
                        as="select"
                        id="specialty"
                        name="specialty"
                        className="hover:border-sky-800 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm text-center"
                        onChange={handleChange}
                        value={values.specialty}
                      >
                        <option value="">Spécialité</option>
                          { specialitiesJson &&
                            Object.entries(specialitiesJson).map(([key, value]) => (
                            <option key={value} value={value} className="">
                              {key}
                            </option>
                          ))}
                      </Field>
                    </div>
                  ) : null}
                </div>
                <div>
                  <TextFieldWithDynamicPlaceholderAdresse
                    id="adresse"
                    name="adresse"
                    placeholderText="Ajouter une localisation"
                    autoComplete="off"
                  />
                  <ErrorMessage name="adresse" component="div"/>
                </div>
                <div className="flex flex-col justify-center items-center mt-16">
                  <button
                    type="submit"
                    disabled={isSubmitting}
                    className="w-full flex justify-center items-center btn btn-primary py-[14px]"
                  >
                    Rechercher
                  </button>
                </div>
              </Form>
              {errors.atLeastTwoFields && (
                <div>
                  <h5 className="text-red-500 fadeIn mt-5 p-4 bg-red-100 rounded-3xl text-center">
                    {errors.atLeastTwoFields}
                  </h5>
                </div>
              )}
            </div>
          )}
        </section>
      )}
    </Formik>
  );
};

export default SearchPsForm;
