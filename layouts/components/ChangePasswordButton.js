import { useSession } from "next-auth/react";
import { useRouter } from "next/router";

const ChangePasswordButton = () => {
  const router = useRouter();
  const { data: session } = useSession();

  // Fonction pour rediriger vers la page de changement de mot de passe
  const handleChangePassword = () => {
    if (session) {
      const clientID = process.env.NEXT_PUBLIC_CLIENT_ID;
      const redirectURI = encodeURIComponent(process.env.NEXT_PUBLIC_KEYCLOAK_REDIRECT_URL_PWD);
      const keycloakServer = process.env.NEXT_PUBLIC_ISSUER
      const keycloakURL = `${keycloakServer}/protocol/openid-connect/auth?client_id=${clientID}&redirect_uri=${redirectURI}&response_type=code&scope=openid&kc_action=UPDATE_PASSWORD`;
      // const keycloakLocalURL = `http://localhost:9092/realms/Toobib/protocol/openid-connect/auth?client_id=${clientID}&redirect_uri=${redirectURI}&response_type=code&scope=openid&kc_action=UPDATE_PASSWORD`;
      router.push(keycloakURL);
    } else {
      router.push("/connexion");
    }
  };

  return (
    <button className="btn btn-primary z-0 py-[14px] md:w-full w-full flex items-center text-center justify-center" onClick={handleChangePassword}>Changer le mot de passe</button>
  );
};

export default ChangePasswordButton;


