import React, {useCallback, useState} from 'react';
import {useField, useFormikContext} from 'formik';
import debounce from 'lodash.debounce';
const TextFieldWithDynamicPlaceholderToobib = ({ id, placeholderText, ...props }) => {
  const [field, meta] = useField(props);
  const [placeholder, setPlaceholder] = useState(placeholderText);
  const [showDropdown, setShowDropdown] = useState(false);
  const [showSlider, setShowSlider] = useState(false);
  const [rangeValue, setRangeValue] = useState(50);
  const [position, setPosition] = useState({ latitude: null, longitude: null });
  const [error, setError] = useState(null);
  const { setFieldValue, values } = useFormikContext();
  const [cities, setCities] = useState([]);

  const handleChange = (event) => {
    const userInput = event.target.value;
    setFieldValue('adresse', userInput)
    setFieldValue(userInput);

    if (userInput.length >= 3) {
      fetchCities(userInput);
    } else {
      setCities([]);
    }
  };

  const handleSelectOption = (option) => {
    if (option === 'Autour de moi') {
      setFieldValue('adresse', '')
      setCities(null)
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            const {latitude, longitude} = position.coords;
            setPosition({latitude, longitude});
            setFieldValue('position', {latitude, longitude});
          },
          (error) => {
            setError("Veuillez autoriser la localisation de votre navigateur afin de sélectionner cette option");
          },
        );
      } else {
        setError("Geolocation is not supported by this browser.");
      }
      if (position !== null) {
        setShowSlider(true)
        setPlaceholder('Autour de moi')
        setShowDropdown(false);
      } else {
        setError("Veuillez autoriser la localisation de votre navigateur afin de sélectionner cette option")
      }
    }
    if (option !== 'Autour de moi') {
      setShowSlider(false)
      setFieldValue('rangeAround', '')
      setFieldValue('adresse', option)
      setPlaceholder(option)
    }
  };

  const handleFocus = () => {
    setPlaceholder('');
    setShowDropdown(true);
    setShowSlider(false)
    setRangeValue(null)

    const userInput = event.target.value;
    setFieldValue('adresse', userInput)
    setFieldValue(userInput);

    if (userInput.length >= 3) {
      fetchCities(userInput);
    } else {
      setCities([]);
    }

  };

  const handleBlur = (e) => {
    if (field.value === '') {
      setPlaceholder(placeholderText);
      setError(null)
      setCities(null)
    }

    if (field.value === 'Autour de moi') {
      setPlaceholder(placeholderText);
      setError(null)
      setCities(null)
    }

    setTimeout(() => setShowDropdown(false), 200);
    if(field.value !== 'Autour de moi'){
      setFieldValue('rangeAround', '')
    }
    field.onBlur(e);
  };

  const fetchCities = useCallback(debounce((userInput) => {
    fetch(`https://geo.api.gouv.fr/communes?nom=${userInput}&boost=population&limit=4&fields=code,nom,codesPostaux,centre`)
      .then(response => response.json())
      .then(data => {
        const citiesArray = data.map(city => ({
          adresse: city.nom,
          longitude: city.centre.coordinates[0],
          latitude: city.centre.coordinates[1]
        }));
        setCities(citiesArray);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, 200), []);

  return (
    <div className="relative mt-10 ">
      {error ? (
          <p className="text-center text-red-500 fadeIn">
            {error}
          </p>
      ) : (
        null
      )}
      <input
        className="hover:border-sky-800  block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm text-center"
        {...field}
        {...props}
        id={id}
        placeholder={placeholder}
        value={values.adresse}
        onFocus={handleFocus}
        onBlur={handleBlur}
        onChange={handleChange}
      />
      {meta.touched && meta.error ? (
        <div className="error">{meta.error}</div>
      ) : null}
      {showDropdown && (
        <div className="absolute bg-white border border-gray-300 rounded-md shadow-sm mt-1 w-full z-10">
          <div
            className="py-2 px-4 hover:bg-gray-100 cursor-pointer text-center flex items-center justify-center"
            onClick={() => handleSelectOption('Autour de moi')}
          >
            <svg data-slot="icon" fill="none" strokeWidth="1.5" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" width="20" height="20" className="mr-2">
              <path strokeLinecap="round" strokeLinejoin="round" d="M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"></path>
              <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1 1 15 0Z"></path>
            </svg>
            Autour de moi
          </div>
          {cities && cities.length > 0 && cities.map((city, index) => (
            <div
              key={index}
              className="py-2 px-4 hover:bg-gray-100 cursor-pointer text-center"
              onClick={() => handleSelectOption(city.adresse)}
            >
              {city.adresse}
            </div>
          ))}
        </div>
      )}
      {showSlider && (
        <div className="mt-12 fadeIn flex flex-col justify-center items-center">

          <input
            type="range"
            min="1"
            max="50"
            className="slider"
            id="rangeAround"
            name="rangeAround"
            value={values.rangeAround}
            onChange={(e) => setFieldValue("rangeAround", e.target.value)}
          />
          <p className="flex flex-col text-center mt-5 text-xl font-extrabold text-gray-950">
            Dans un rayon de : {values.rangeAround} km
          </p>
        </div>
      )}
    </div>
  );
};

export default TextFieldWithDynamicPlaceholderToobib;
