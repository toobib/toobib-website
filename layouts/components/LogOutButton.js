import { useSession, signOut as nextAuthSignOut } from "next-auth/react";
import { useRouter } from "next/router";
import React from "react";

const LogOutButton = () => {
  const router = useRouter();
  const { data: session } = useSession();

  const handleLogout = () => {
    const clientID = process.env.NEXT_PUBLIC_CLIENT_ID;
    const redirectURI = encodeURIComponent(process.env.NEXT_PUBLIC_KEYCLOAK_REDIRECT_URL_LOGOUT);
    const keycloakServer = process.env.NEXT_PUBLIC_KEYCLOAK_ISSUER_LOGOUT;
    const clientId = process.env.NEXT_PUBLIC_CLIENT_ID;
    let keycloakURL = `${keycloakServer}/protocol/openid-connect/logout?post_logout_redirect_uri=${redirectURI}&client_id=${clientID}`;

    if (session){
      nextAuthSignOut({ redirect: false }).then(() => {
        router.push(keycloakURL);
      });
    } else {
      router.push("/connexion");
    }
  };

  return (
    <button className="btn btn-primary z-0 py-[14px]" onClick={handleLogout}>
      Se déconnecter
    </button>
  );
};

export default LogOutButton;
