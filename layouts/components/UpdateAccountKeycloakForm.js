import React, { useState, useEffect } from "react";
import CabinetField from "./CabinetField";
import Link from "next/link";
import getProfessionCodes from "@lib/api/NOS/getProfessionCodes";
import Loading from "@components/Loading";
const UpdateAccountKeycloakForm = ({ account, initialValues, formik }) => {
  const [professionOptions, setProfessionOptions] = useState([]);
  const [errorFhir, setErrorFhir] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchProfessions = async () => {

      const professions = await getProfessionCodes();
      const professionsArray = professions.map((code) => code.display)
      setProfessionOptions(professionsArray);
      setLoading(false);
    };

    fetchProfessions();
  }, []);

  // Populate the initialValues with the Cabinet fields
  if (account.attributes) {
    Object.keys(account.attributes.Cabinet).forEach((id) => {
      initialValues.Cabinet[id] = account.attributes.Cabinet[id];
    });
  }

  // Function to handle the click event of the "Add Field" button
  const handleAddFieldClick = () => {
    // Convert the keys to numbers and find the maximum key
    let maxId = Math.max(...Object.keys(formik.values.Cabinet).map(Number));
    if (maxId === -Infinity) {
      maxId = 0;
    }
    // Generate a new unique id for the new cabinet
    const newId = maxId + 1;
    // Add a new empty cabinet to the formik.values.Cabinet object
    formik.setFieldValue(`Cabinet.${newId}`, {
      Lieu: " ",
      Adresse: " ",
      CodePostal: " ",
      Ville: " ",
      PhoneLieuConsultation: " ",
      Etage: " ",
      Ascenceur: " ",
      AccesHandicapes: " ",
      Autre: " ",
    });
  };

  if(loading){
    return <Loading></Loading>;
  }

  return (
      <form
      onSubmit={formik.handleSubmit}
      className="space-y-6 w-full mx-auto p-6 bg-white shadow-lg rounded-lg"
    >
      <h3 className="text-center text-sky-800 mb-8">
        Informations personnelles
      </h3>
      <div className="sm:grid sm:grid-cols-2 sm:space-y-0 gap-6 column space-y-6">
        <input
          id="id"
          name="id"
          type="text"
          hidden
          defaultValue={formik.values.id}
        />
        <input
          id="username"
          name="username"
          type="text"
          hidden
          defaultValue={formik.values.username}
        />
        <div className="col-span-1">
          <label
            htmlFor="firstName"
            className="block text-sm font-medium text-gray-700"
          >
            Prénom
          </label>
          <input
            id="firstName"
            name="firstName"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.firstName}
            className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>

        <div className="col-span-1">
          <label
            htmlFor="lastName"
            className="block text-sm font-medium text-gray-700"
          >
            Nom de famille
          </label>
          <input
            id="lastName"
            name="lastName"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.lastName}
            className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>

        <div className="col-span-1">
          <label
            htmlFor="PractionerRole"
            className="block text-sm font-medium text-gray-700"
          >
            Profession{" "}
          </label>
          <select
            id="PractionerRole"
            name="PractionerRole"
            onChange={formik.handleChange}
            value={formik.values.PractionerRole}
            className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          >
            <option value="">Sélectionnez une profession</option>
            {Array.isArray(professionOptions) ? (
              professionOptions.map((profession, index) => (
                <option key={index} value={profession}>
                  {profession}
                </option>
              ))
            ) : (
              <option value="">Loading...</option>
            )}
          </select>
        </div>

        <div className="col-span-1">
          <label
            htmlFor="PractionerRole"
            className="block text-sm font-medium text-gray-700"
          >
            Numéro RPPS{" "}
          </label>
          <input
            id="rppsId"
            name="rppsId"
            type="text"
            disabled
            onChange={formik.handleChange}
            value={formik.values.rppsId}
            className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>

        <div className="col-span-1">
          <label
            htmlFor="Phone"
            className="block text-sm font-medium text-gray-700"
          >
            Numéro de téléphone
          </label>
          <input
            id="Phone"
            name="Phone"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.Phone}
            className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>

        <div className="col-span-1">
          <label
            htmlFor="email"
            className="block text-sm font-medium text-gray-700"
          >
            Email
          </label>
          <input
            id="email"
            name="email"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.email}
            className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>
        <div className="col-span-1">
          <label
            htmlFor="conventionnement"
            className="block text-sm font-medium text-gray-700"
          >
            Conventionnement
          </label>
          <input
            id="Conventionnement"
            name="Conventionnement"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.Conventionnement}
            className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>
        <h3 className="col-span-2 text-center text-sky-800 mb-10 mt-10">
          Adresse(s)
        </h3>
        {Object.keys(formik.values.Cabinet).map((id) => (
          <CabinetField key={id} id={id} formik={formik} />
        ))}
        <div className="col-span-2 text-center flex items-center justify-center mb-10 mt-10">
          <button
            type="button"
            className="flex flex-col items-center text-sky-800 mb-10 mt-10 space-y-2"
            onClick={handleAddFieldClick}
          >
            <span className="text-xl font-bold">
              Ajouter un lieu de consultation
            </span>
            <svg
              data-slot="icon"
              fill="none"
              strokeWidth="3"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
              aria-hidden="true"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M12 4.5v15m7.5-7.5h-15"
              />
            </svg>
          </button>
        </div>
      </div>
      <div className="flex justify-center">
        <div className="flex flex-col justify-center items-center max-w-sm space-y-4">
          <button
            type="submit"
            className="w-full flex justify-center items-center btn btn-primary py-[14px]"
          >
            Sauvegarder
          </button>
          <Link
            href="/mon-profil"
            className="w-full flex justify-center items-center btn btn-primary-cancel py-[14px]"
          >
            Annuler
          </Link>
        </div>
      </div>
    </form>
      )
    };
export default UpdateAccountKeycloakForm;
