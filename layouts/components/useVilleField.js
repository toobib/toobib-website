import { useState, useEffect } from 'react';

const useVilleField = (formikContext) => {
  const { setFieldValue, values } = formikContext;
  const [ville, setVille] = useState(values.ville);

  useEffect(() => {
    setFieldValue('ville', ville);
  }, [ville, setFieldValue]);

  return [ville, setVille];
};

export default useVilleField;
