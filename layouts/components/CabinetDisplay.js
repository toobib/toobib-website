import React from "react";
import prefixes from "@config/prefixes.json";
const CabinetDisplay = ({ data, id }) => {
  return (
    <div>
      <br />
      {Object.entries(data).map(([key, value]) => (
        <div key={key}>
          {value ? (
            <p className="text-md text-gray-600">
              {prefixes.monprofil[key]}
              {value}
            </p>
          ) : (
            ""
          )}
        </div>
      ))}
    </div>
  );
};

export default CabinetDisplay;
