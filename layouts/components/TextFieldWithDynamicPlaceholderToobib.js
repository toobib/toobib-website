import React, { useState } from 'react';
import { useField } from 'formik';

const TextFieldWithDynamicPlaceholderToobib = ({ id, placeholderText, ...props }) => {
  const [field, meta] = useField(props);
  const [placeholder, setPlaceholder] = useState(placeholderText);

  const handleFocus = () => {
    setPlaceholder('');
  };

  const handleBlur = (e) => {
    if (field.value === '') {
      setPlaceholder(placeholderText);
    }
    field.onBlur(e);
  };

  return (
    <div>
      <input
        className="hover:border-sky-800 mt-10 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm text-center"
        {...field}
        {...props}
        id={id}
        placeholder={placeholder}
        onFocus={handleFocus}
        onBlur={handleBlur}
      />
      {meta.touched && meta.error ? (
        <div className="error">{meta.error}</div>
      ) : null}
    </div>
  );
};

export default TextFieldWithDynamicPlaceholderToobib;
