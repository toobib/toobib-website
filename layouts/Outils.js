import { markdownify } from "@lib/utils/textConverter";
import Image from "next/image";
import { useState } from "react";
import Link from "next/link";

function Outils({ data }) {
  const {
    frontmatter: { title, description, articles },
  } = data;

  //read more
  const [isReadMore, setIsReadMore] = useState(articles.map(() => false));
  const toggleReadMore = (index) => {
    const newIsReadMore = [...isReadMore];
    newIsReadMore[index] = !newIsReadMore[index];
    setIsReadMore(newIsReadMore);
  };
  return (
    <>
      <section className="section">
        <div className="container">
          {markdownify(title, "h1", "text-center font-normal")}
          <div className="grid grid-cols-2 lg:grid-cols-3">
            {articles.map((article, index) => (
        article.url ? (
            <Link href={article.url} className="block">
              <div key={index} className="col-span-3 mt-6 md:col-span-1">
                <div className="p-12 shadow flex flex-col justify-between">
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      height: "100%",
                    }}
                  >
                    <Image
                      className="my-2"
                      height={128}
                      width={128}
                      src={article.logo}
                      alt={article.title}
                    />
                  </div>
                  <div className="text-center">
                    {markdownify(article.title, "h3", "text-center")}
                  </div>
                  <div className="my-6 md:min-h-60 lg:min-h-72 xl:min-h-64">
                    <div>
                        {markdownify(article.description)}
                    </div>
                  </div>
                </div>
              </div>
        </Link> ) : (
              <div key={index} className="col-span-3 mt-6 md:col-span-1">
                <div className="p-12 shadow flex flex-col justify-between">
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      height: "100%",
                    }}
                  >
                    <Image
                      className="my-2"
                      height={128}
                      width={128}
                      src={article.logo}
                      alt={article.title}
                    />
                  </div>
                  <div className="text-center">
                    {markdownify(article.title, "h3", "text-center")}
                  </div>
                  <div className="my-6 md:min-h-60 lg:min-h-72 xl:min-h-64">
                    <div>
                        {markdownify(article.description)}
                    </div>
                  </div>
                </div>
              </div>
        )
            ))}
          </div>
        </div>
      </section>
    </>
  );
}

export default Outils;
