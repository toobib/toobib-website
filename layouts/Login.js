"use client";
import Link from "next/link";
import Cta from "./components/Cta";
// import { markdownify } from "@lib/utils/textConverter";
import { signIn, signOut, useSession } from "next-auth/react";
// import { getServerSession } from "next-auth";
// import { authOptions } from "@pages/api/auth/[...nextauth]";
// import { useEffect, useState } from "react";

function Login({ data }) {

  const { data: session } = useSession();

  const {
    frontmatter: { title, post_scriptum, articles },
  } = data;

  if (session) {
    return (
      <>
        <section className="section">
          <div className="container utilities-body py-44 pb-96">
            <h1 className="text-center font-normal">Bienvenue sur Toobib, {session.user.name} !</h1>
          </div>
        </section>
      </>
    );
  }
  return (
    <>
      <section className="section">
        <div className="container utilities-body py-44 pb-96">
          <h1 className="text-center font-normal">{title}</h1>
          <div className="flex flex-col justify-center items-center space-y-4 section p-12  mt-6">
            <button
              className="btn btn-primary flex items-center justify-center w-72"
              onClick={() => signIn("keycloak")}>
              Se connecter
            </button>
          </div>
        </div>
      </section>
    </>
  );
}

export default Login;
