import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import SearchPsForm from "./components/SearchPsForm";
import getSearchResults from "@lib/api/SearchApi/getSearchResults";
import getPractitionerAndPractitionerRole from "@lib/api/FHIR/getPractitionerAndPractitionerRole";

const Search = () => {
  const minimumPage = 1;
  const _count = 12; // Nombre de résultats par page
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [maximumPage, setMaximumPage] = useState(1);
  const [requestValues, setRequestValues] = useState(null);
  const [responseSearchAPI, setResponseSearchAPI] = useState(null);
  const [entries, setEntries] = useState([]); // Nouvel état pour les résultats paginés
  const router = useRouter();

  useEffect(() => {
    // if stop the inside of the useEffect to run at ComponentMount
    if(requestValues) {
      const fetchPageData = async () => {
        setLoading(true);
        setEntries(Array.from({length: _count}, () => {}));
        const resultValues = await getSearchResults(requestValues);
        setResponseSearchAPI(resultValues);
        setMaximumPage(Math.ceil(resultValues.total / _count));
        setEntries(resultValues.entry);
        setLoading(false);
    };
    fetchPageData();
    }
  }, [requestValues]);

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  return (
    <>
      <SearchPsForm
        onSubmit={(values) => {
          setRequestValues({...values, _count: _count});
          setCurrentPage(1);
        }}
      />
      <section className="section bg-theme-light">
        <div className="container max-w-[1440px]">
          {responseSearchAPI === undefined && (
            <div>
              <h5 className="text-red-500 fadeIn mt-5 mb-5 p-4 bg-red-100 rounded-3xl text-center">
                Notre fonction de recherche est momentanément ralentie et ne fonctionne pas pleinement.
                Veuillez nous excuser pour la gêne occasionnée.
                Si le problème persiste,
                <a
                  href="/contact"
                  className="text-blue-500 underline hover:text-blue-800"
                >
                  contactez-nous
                </a>
              </h5>
            </div>
          )}

          {Array.isArray(responseSearchAPI) && responseSearchAPI.total > 0 && (
            <div className="text-center">
              <h2>{responseSearchAPI.total} résultats pour votre recherche</h2>
            </div>
          )}

          <div className="fadeIn h-full mt-8 grid gap-x-8 gap-y-6 sm:grid-cols-2 lg:grid-cols-4">
            {entries.map((practitioner, index) => (
              <div
                key={index}
                className={`fadeIn feature-card rounded-xl bg-white p-5 pb-8 text-center shadow-sm mt-7 border-4 border-sky-800 `}
              >
                <div className="h-60">
                  <p className={`font-extrabold text-3xl text-sky-800 ${loading ? "skeleton skeleton-text mx-auto" : ""}`}>
                    {loading ? "" : practitioner.nom}
                  </p>
                  <p className={`font-extrabold text-3xl text-sky-800 mb-8 capitalize ${loading ? "skeleton skeleton-text mx-auto" : ""}`}>
                    {loading ? "" : practitioner.prenom.toLowerCase()}
                  </p>
                  <p className={`font-extrabold text-xl text-gray-950 ${loading ? "skeleton skeleton-text skeleton-text__body mx-auto" : ""}`}>
                    {loading ?
                      ("")
                        :(
                        <>
                      {practitioner.profession != 'Médecin' ?
                        `${practitioner.profession}`
                          :
                        `${practitioner.savoir_faire}`}
                      {practitioner.commune ? ` à ${practitioner.commune}`: ''}
                      </>
                    )}
                  </p>
                  {loading ? "" : practitioner.distance && (
                    <p className={`font-extrabold text-xl text-gray-950 ${loading ? "skeleton skeleton-text skeleton-text__body mx-auto" : ""}`}>
                      à {(practitioner.distance / 1000).toFixed(1)} km de vous
                    </p>
                  )}
                  <br />
                </div>
                <button
                  onClick={() =>
                    router.push({
                      pathname: "/toobib-profil",
                      query: { data: JSON.stringify(practitioner) },
                    })
                  }
                  className={`w-full flex justify-center items-center btn ${loading ? "btn-not-allowed" : "btn-primary"} py-[14px]`}
                >
                  Consulter
                </button>
              </div>
            ))}
          </div>
        </div>
        {responseSearchAPI && responseSearchAPI.total > _count && (
          <div className="flex justify-center mt-8">
            {currentPage > minimumPage && (
              <button
                onClick={async () => {
                  const newPage = currentPage - 1;
                  paginate(newPage);
                  const newValues = {...requestValues, _page: newPage, _count: _count}
                  setRequestValues(newValues);
                  }
                }
                className="btn btn-secondary mx-2"
              >
                Page précédente
              </button>
            )}

            {currentPage < maximumPage && (
              <button
                onClick={async () => {
                  const newPage = currentPage + 1;
                  paginate(newPage);
                  const newValues = {...requestValues, _page: newPage, _count: _count}
                  setRequestValues(newValues);
                  }
                }
                className="btn btn-secondary mx-2"
              >
                Page suivante
              </button>
            )}
          </div>
        )}
      </section>
    </>
  );
};

export default Search;
