import React from "react";
import { useState, useEffect } from "react";
import { signIn, useSession } from "next-auth/react";
import UpdateAccountKeycloakForm from "./components/UpdateAccountKeycloakForm";
import updateAccount from "@lib/api/Keycloak/AccountApi/updateAccount";
import getAccount from "@lib/api/Keycloak/AccountApi/getAccount";
import Loading from "./components/Loading";
import getPractitionerAndPractitionerRole from "@lib/api/FHIR/getPractitionerAndPractitionerRole";
import getOrganizationById from "@lib/api/FHIR/getOrganizationById";
import { useRouter } from "next/router";
import getProfessionCodes from "@lib/api/NOS/getProfessionCodes";
import { useFormik } from "formik";
import {
  extractOrgIdFromFhirPractitionerBundle,
  getFormatedOrganizationData,
} from "@lib/helpers/organizationHelperFunctions";

const ModifierProfile = () => {
  const { data: session } = useSession();
  const router = useRouter();
  const [account, setAccount] = useState(null);
  const [initialValues, setInitialValues] = useState({
    firstName: "",
    lastName: "",
    PractionerRole: "",
    rppsId: "",
    Phone: "",
    Conventionnement: "",
    Cabinet: {},
    email: "",
    id: "",
    username: "",
  });
  const [errorNotif, setErrorNotif] = useState(false);
  const [errorFhir, setErrorFhir] = useState(false);
  const [loading, setLoading] = useState(true);
  const [professionOptions, setProfessionOptions] = useState();

  useEffect(() => {
    const fetchProfessions = async () => {
      try {
        const professionCodes = await getProfessionCodes();
        const professionArray = professionCodes.map((code) => code.display);
        setProfessionOptions(professionArray);
        handleProfessionOptionsSet(professionArray);
      } catch (error) {
        setErrorFhir("Error fetching professions");
      } finally {
        setLoading(false);
      }
    };

    fetchProfessions();
  }, []);

  const handleProfessionOptionsSet = (results) => {
    if (results.length === 0 || results.length === undefined) {
      setErrorFhir(true);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      if (session) {
        let response = await getAccount(session.accessToken);
        if (response && response.attributes === undefined) {
          setInitialValues({
            firstName: response.firstName,
            lastName: response.lastName,
            PractionerRole: " ",
            rppsId: " ",
            Phone: " ",
            Conventionnement: " ",
            Cabinet: {},
            email: response.email,
            id: response.id,
            username: response.username,
          });
        }
        if (response && response.attributes) {
          response = {
            ...response,
            attributes: {
              ...response.attributes,
              Cabinet: JSON.parse(
                response.attributes.Cabinet === undefined
                  ? "{}"
                  : response.attributes.Cabinet[0],
              ),
            },
          };
          setInitialValues({
            firstName: response.firstName,
            lastName: response.lastName,
            PractionerRole: response.attributes.PractionerRole,
            rppsId: response.attributes.rppsId,
            Phone: response.attributes.Phone,
            Conventionnement: response.attributes.Conventionnement,
            Cabinet: {},
            email: response.email,
            id: response.id,
            username: response.username,
          });
        }
        setAccount(response);
      }
    };

    fetchData();
  }, [session]);

  const formik = useFormik({
    initialValues: initialValues,
    onSubmit: async (values) => {
      await updateAccount(values, session.accessToken);
      router.push("/mon-profil");
    },
    enableReinitialize: true,
  });

  const handleFhirClick = async () => {
    const practitioner = await getPractitionerAndPractitionerRole("0039100730");

    if (practitioner.error) {
      setErrorNotif(true);
      return;
    }
    setErrorNotif(false);
    let professionCode;
    practitioner.data?.entry[1]?.resource?.code[0]?.coding.forEach((code) => {
      if (parseInt(code.code)) {
        professionCode = code.code;
      }
    });
    const professionCodes = await getProfessionCodes();
    const profession = professionCodes.find((code) => {if (code.coding === professionCode) {return code.display}})
    const orgId = extractOrgIdFromFhirPractitionerBundle(practitioner.data);
    let fhirData = {};

    if (orgId) {
      orgId.forEach(async (id, index) => {
        const org = await getFormatedOrganizationData(id);
        formik.setFieldValue(`Cabinet.${index + 1}.Lieu`, org.Lieu);
        formik.setFieldValue(`Cabinet.${index + 1}.CodePostal`, org.CodePostal);
        formik.setFieldValue(`Cabinet.${index + 1}.Adresse`, org.Adresse);
        formik.setFieldValue(
          `Cabinet.${index + 1}.PhoneLieuConsultation`,
          org.PhoneLieuConsultation,
        );
        formik.setFieldValue(`Cabinet.${index + 1}.Ville`, org.Ville);
      });
    }
    fhirData = { ...fhirData, profession: profession };
    formik.setFieldValue("PractionerRole", fhirData.profession);
  };

  if (!account) {
    return (
      <>
        <section className="section">
          <div className="container utilities-body py-44 pb-96">
            <h1 className="text-center font-normal">Veuillez vous connecter</h1>
            <div className="flex flex-col justify-center items-center space-y-4 section p-12  mt-6">
              <button
                className="btn btn-primary flex items-center justify-center w-72"
                onClick={() => signIn("keycloak")}
              >
                Se connecter
              </button>
            </div>
          </div>
        </section>
      </>
    );
  }
  if (errorFhir) {
    return <h1>test</h1>;
  }

  if (session) {
    return (
      <>
        <section className="section">
          <h1 className="text-center font-normal mb-20">Modifier mon profil</h1>

          {errorNotif && (
            <div>
              <h5 className="text-red-500 fadeIn mt-5 mb-5 p-4 bg-red-100 rounded-3xl text-center">
                Nous utilisons l'API « Annuaire Santé » de l'ANS, qui semble
                être indisponible, réessayez plus tard. Pour plus
                d'informations&nbsp;
                <a
                  href="https://status.annuaire-sante.esante.gouv.fr/"
                  className="text-blue-500 underline hover:text-blue-800"
                >
                  status.annuaire-sante
                </a>
              </h5>
            </div>
          )}

          <div className="flex flex-col items-center justify-center">
            <div className="flex flex-col items-center justify-center md:w-3/3">
              <button
                className="btn btn-primary w-4/5"
                onClick={handleFhirClick}
              >
                {/* eslint-disable-next-line react/no-unescaped-entities */}
                Remplir le formulaire grâce à l'annuaire des professionel.les de
                santé
              </button>
              <UpdateAccountKeycloakForm
                account={account}
                initialValues={initialValues}
                formik={formik}
              />
            </div>
          </div>
        </section>
      </>
    );
  }
};

export default ModifierProfile;
