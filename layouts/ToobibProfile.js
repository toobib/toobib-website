import React, { useState } from "react";
import { useRouter } from "next/router";
import getPractitionerAndPractitionerRole from "@lib/api/FHIR/getPractitionerAndPractitionerRole";
import getProfessionString from "@lib/api/FHIR/getProfessionString";
import { useEffect } from "react";
import Image from "next/image";
import img from "../public/images/blog-6.jpg";
import {
  getFormatedOrganizationData,
} from "@lib/helpers/organizationHelperFunctions";
import Link from "next/link";
import ChangePasswordButton from "@components/ChangePasswordButton";
import CabinetDisplay from "./components/CabinetDisplay";
import getPractitionerDataFromToobibDB from "@lib/api/toobibData/helperFunctions/getPractitionerDataFromToobibDB"

const ToobibProfile = () => {
  const router = useRouter();
  const [profil, setProfil] = useState({});
  const [cabinets, setCabinets] = useState(null);
  const [caldav, setCaldav] = useState(false);
  const [consultation, setConsultation] = useState(false);
  const [onmap, setOnmap] = useState(false);


  useEffect(() => {
    const fetchPractitioner = async () => {
      if (router.query.data) {
        const parameterData = JSON.parse(router.query.data);
        const response = await getPractitionerAndPractitionerRole(
          parameterData.inpp,
        );


        let dictOrganizationBoundToProfession = {};
        for (const entry of response.data.entry.slice(1)) {
          console.log(entry);
          for (const coding of entry.resource.code[0].coding) {
            const professionCode = coding.code;
            const professionString = await getProfessionString(professionCode);
            if (!professionString || !entry.resource?.organization?.reference) continue;
            if (dictOrganizationBoundToProfession[professionString] == undefined) {
              dictOrganizationBoundToProfession[professionString] = [];
            }
            dictOrganizationBoundToProfession[professionString].push(entry.resource.organization.reference.split?.("/")?.[1])
          }
        }

        if (dictOrganizationBoundToProfession != {}) {
          for (const profession in dictOrganizationBoundToProfession) {
            dictOrganizationBoundToProfession[profession] = await Promise.all(dictOrganizationBoundToProfession[profession].map(async (orgaId, index) => {
              const org = await getFormatedOrganizationData(orgaId);
              return {
                id: index,
                data: {
                  Lieu: org.Lieu,
                  CodePostal: org.CodePostal,
                  Adresse: org.Adresse,
                  PhoneLieuConsultation: org.PhoneLieuConsultation,
                  Ville: org.Ville,
                },
              };
            }))
          }
        }

        const practitionerDataFromToobibDB = getPractitionerDataFromToobibDB(parameterData.inpp)
        setCabinets(dictOrganizationBoundToProfession);
        setProfil({
          firstName: parameterData.prenom,
          lastName: parameterData.nom,
          practionerRole: parameterData.profession,
          savoirFaire: parameterData.savoir_faire,
          phone: practitionerDataFromToobibDB.phone,
          email: practitionerDataFromToobibDB.email,
          conventionnement: practitionerDataFromToobibDB.conventionnement,
        });
      }
    };
    fetchPractitioner();
  }, [router.query.data]);
  return (
    <>
      <section className="section">
        <div className="flex flex-col items-center justify-center mb-10">
          <div className="xl:w-2/3 xl:space-x-6 xl:flex-row w-full p-6 rounded-lg bg-white flex flex-col items-center justify-center space-y-6">
            <div className="sm:shadow-lg rounded-2xl bg-white sm:p-6 flex flex-col md:flex-row items-center">
              <div className="flex flex-col items-center justify-center md:justify-between space-y-6 mb-10">
                <div className="flex flex-col items-center justify-start">
                  <Image
                    className="rounded-full h-48 w-48"
                    src={img}
                    alt="Avatar"
                  />
                  <div className="flex flex-col items-center justify-end mt-10">
                    <Link
                      className={`btn ${caldav === true ? 'btn-primary' : 'btn-not-allowed'} mb-5 z-0 py-[14px] md:w-full w-full flex items-center text-center justify-center`}
                      href="#"
                      rel=""
                    >
                      Prendre rendez-vous
                    </Link>
                    <Link
                      className={`btn ${consultation === true ? 'btn-primary' : 'btn-not-allowed'} mb-5 z-0 py-[14px] md:w-full w-full flex items-center text-center justify-center`}
                      href="#"
                      rel=""
                    >
                      Commencer la téléconsultation
                    </Link>
                    <Link
                      className={`btn ${onmap === true ? 'btn-primary' : 'btn-not-allowed'} mb-5 z-0 py-[14px] md:w-full w-full flex items-center text-center justify-center`}
                      href="#"
                      rel=""
                    >
                      Afficher sur la carte
                    </Link>
                  </div>
                </div>
              </div>
              <div className="text-left md:ml-6">
                <h3 className="text-xl font-bold profileTitle mb-6">
                  {profil.firstName} {profil.lastName}
                </h3>
                <p className="text-md text-gray-600 ">
                  {profil.practionerRole}
                </p>
                { profil?.savoirFaire && <p className="text-md text-gray-600 ">{profil.savoirFaire}</p>
                }
                <br />
                <p className="text-md text-gray-600">
                  {profil?.Phone && (!cabinets || !cabinets.some((cabinet) => cabinet.data.PhoneLieuConsultation === profil?.Phone)) ? `Numéro de téléphone: ${profil.Phone}` : ''}
                </p>
                <p className="text-md text-gray-600">
                  {profil?.email ? `Adresse e-mail : ${profil.email}` : ''}
                </p>
                <p className="text-md text-gray-600 mt-2 mb-6">
                  { profil?.conventionnement ? `Conventionnement : ${profil.conventionnement}` : ""}
                </p>
                {cabinets && Object.keys(cabinets).length > 0 ? (
                  <>
                    <h3 className="text-xl font-bold profileTitle mb-6">
                      Lieu(x) de consultation
                    </h3>
                    <div>
                      {Object.keys(cabinets).map((profession) => (
                        <div>
                          <h4 className="font-extrabold">{profession} :</h4>
                          {cabinets[profession].map((cabinet) => (
                            <>
                            <CabinetDisplay
                              key={cabinet.id}
                              id={cabinet.id + 1}
                              data={cabinet.data}
                            />
                              {cabinet.id + 1 === cabinets[profession].length && <br />}
                              </>
                          ))}
                        </div>
                      ))}
                    </div>
                  </>
                ) : (
                  <>
                    <h3 className="text-xl font-bold profileTitle mb-6">
                      Aucun lieu de consultation
                    </h3>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default ToobibProfile;
