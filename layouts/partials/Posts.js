import { useState } from 'react';
import config from "@config/config.json";
import Image from "next/image";
import Link from "next/link";


const Description = ({ post, descLimit, blog_folder }) => {
  const [isExpanded, setIsExpanded] = useState(false);

  const handleToggle = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <p className="text-text">
      {isExpanded ? post.frontmatter.desc : `${post.frontmatter.desc.slice(0, descLimit)}...`}
      {post.frontmatter.desc.length > descLimit && (
        <span onClick={handleToggle} className="text-primary cursor-pointer">
          {isExpanded ? " Réduire" : " En savoir plus"}
        </span>
      )}
    </p>
  );
};

const Posts = ({ posts }) => {
  const { blog_folder } = config.settings;
  const descLimit = 200;

  return (
    <div className="section row pb-0">
      <div className="col-12 pb-12 lg:pb-24">
        <div className="row items-center">
          <div className="col-12 md:col-6">
            {posts[0].frontmatter.image && (
              <Image
                className="h-auto w-full rounded-lg"
                src={posts[0].frontmatter.image}
                alt={posts[0].frontmatter.title}
                width={540}
                height={227}
                priority={true}
              />
            )}
          </div>
          <div className="col-12 md:col-6">
            <h2 className="h3 mb-2 mt-4">
              <Link
                href={`/${blog_folder}/${posts[0].slug}`}
                className="block hover:text-primary"
              >
                {posts[0].frontmatter.title}
              </Link>
            </h2>
            <Description post={posts[0]} descLimit={descLimit} blog_folder={blog_folder} />
            <Link
              className="btn btn-secondary mt-4"
              href={`/${blog_folder}/${posts[0].slug}`}
              rel=""
            >
              Lire l'article
            </Link>
          </div>
        </div>
      </div>
      {posts.slice(1).map((post, i) => (
        <div key={`key-${i}`} className="col-12 mb-8 sm:col-6 lg:col-4">
          {post.frontmatter.image && (
            <Image
              className="rounded-lg"
              src={post.frontmatter.image}
              alt={post.frontmatter.title}
              width={i === 0 ? "925" : "445"}
              height={i === 0 ? "475" : "230"}
            />
          )}
          <h2 className="h3 mb-2 mt-4">
            <Link
              href={`/${blog_folder}/${post.slug}`}
              className="block hover:text-primary"
            >
              {post.frontmatter.title}
            </Link>
          </h2>
          <Description post={post} descLimit={descLimit} blog_folder={blog_folder} />
          <Link
            className="btn btn-secondary mt-4"
            href={`/${blog_folder}/${post.slug}`}
            rel=""
          >
            Lire l'article
          </Link>
        </div>
      ))}
    </div>
  );
};


export default Posts;
