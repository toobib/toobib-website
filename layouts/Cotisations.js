import Link from "next/link";
import Cta from "./components/Cta";
import { markdownify } from "@lib/utils/textConverter";
import useWindowSize from "@components/GetWindowSize";
import { he } from "date-fns/locale";
function Cotisations({ data }) {
  const {
    frontmatter: { title, post_scriptum, articles },
  } = data;

  const { width, height } = useWindowSize();

  if (width < 425) {
    return (
      <>
        <section className="section">
          <div className="container h-full shadow">
            <h1 className="text-center font-normal">{title}</h1>
            <div className="section p-12 ">
              {articles.map((article, index) => (
                <div className="py-6" key={index}>
                  {markdownify(article.title, "h3", "mb-5")}
                  {article.description && markdownify(article.description, "p")}
                  {article.descriptions[0].text !== "helloasso" &&
                    article.descriptions &&
                    markdownify(article.descriptions[0].text, "p")}
                  {article.descriptions[0].text === "helloasso"}
                </div>
              ))}
            </div>
            <iframe
              id="haWidget"
              className="w-full h-full fadeIn"
              data-testid="helloasso-iframe"
              src="https://www.helloasso.com/associations/toobib/adhesions/cotisation-toobib/widget"
              allowFullScreen
            ></iframe>
          </div>
        </section>
      </>
    );
  } else {
    return (
      <>
        <section className="section">
          <div className="container h-full shadow">
            <h1 className="text-center font-normal">{title}</h1>
            <div className="section p-12">
              {articles.map((article, index) => (
                <div className="py-6" key={index}>
                  {markdownify(article.title, "h3", "mb-5")}
                  {article.description && markdownify(article.description, "p")}
                  {article.descriptions[0].text !== "helloasso" &&
                    article.descriptions &&
                    markdownify(article.descriptions[0].text, "p")}
                  {article.descriptions[0].text == "helloasso"}
                </div>
              ))}
            </div>
            <iframe
              id="haWidget"
              className="overflow-visible w-full mt-auto fadeIn"
              data-testid="helloasso-iframe"
              scrolling="no"
              src="https://www.helloasso.com/associations/toobib/adhesions/cotisation-toobib/widget"
            ></iframe>
          </div>
        </section>
      </>
    );
  }
}

export default Cotisations;
