import Link from "next/link";
import Cta from "./components/Cta";
import PeerTubePlayer from "./components/PeerTubePlayer";
import { markdownify } from "@lib/utils/textConverter";

function ToobibPro({ data }) {
  const {
    frontmatter: { title, description, button_solid },
  } = data;
  return (
    <>
      <section className="section">
        <div className="container">
          <h1 className="text-center font-normal">{title}</h1>
          <div className="section p-12 text-center mt-6">
<PeerTubePlayer title="Toobib - Démo" embedKey="d7811615-120f-45bb-ae7e-9d7213862934"/>
<Link
							className="btn btn-primary text-white mt-4"
							href={button_solid.href}
							rel={button_solid.rel}>
								{button_solid.label}
						</Link>
        </div>
        </div>
      </section>
    </>
  );
}

export default ToobibPro;
