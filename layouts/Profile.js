import React, { useEffect, useState } from "react";
import { signIn, useSession } from "next-auth/react";
import getAccount from "@lib/api/Keycloak/AccountApi/getAccount";
import Image from "next/image";
import Link from "next/link";
import Loading from "./components/Loading";
import { useRouter } from "next/router";
import ChangePasswordButton from "./components/ChangePasswordButton";
import ChangePhotoForm from "./components/ChangePhotoForm";
import CabinetDisplay from "./components/CabinetDisplay";
import Login from "./Login";

const Profile = ({ data }) => {
  const { data: session } = useSession();

  const [account, setAccount] = useState(null);
  const router = useRouter();
  const [photoUrl, setPhotoUrl] = useState(" ");

  const [photoUpdated, setPhotoUpdated] = useState(false);
  const [errorNotif, setErrorNotif] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      if (session) {
        const response = await getAccount(session.accessToken);

        setAccount(response);
        if (response && response.attributes) {
          setPhotoUrl(
            process.env.NEXT_PUBLIC_PHOTO_DOMAIN +
              // `/picture/public/profile/0039100730?mode=BytesIO`,
              `/picture/public/profile/${response.attributes.rppsId[0]}?mode=BytesIO&t=${Date.now()}`,
          );
        }
      }
    };
    fetchData();
  }, [session, photoUpdated]);

  const handlePhotoChange = () => {
    setPhotoUpdated((prevState) => !prevState);
  };
  const onPhotoUploadError = () => {
    setErrorNotif(true);
  };

  if (!account) {
    return (
      <>
        <section className="section">
          <div className="container utilities-body py-44 pb-96">
            <h1 className="text-center font-normal">Veuillez vous connecter</h1>
            <div className="flex flex-col justify-center items-center space-y-4 section p-12  mt-6">
              <button
                className="btn btn-primary flex items-center justify-center w-72"
                onClick={() => signIn("keycloak")}
              >
                Se connecter
              </button>
            </div>
          </div>
        </section>
      </>
    );
  }

  if (session) {
    if (account.attributes === undefined) {
      router.push("/modifier-profil");
    }
    if (!account || !account.attributes) {
      return <div>Loading...</div>;
    }
    return (
      <>
        {errorNotif && (
          <div>
            <h5 className="text-red-500 fadeIn mt-5 mb-5 p-4 bg-red-100 rounded-3xl text-center">
              Une erreur s'est produite lors du téléchargement de la photo,
              veuillez réessayer plus tard.
            </h5>
          </div>
        )}

        <section className="section">
          <div className="flex flex-col items-center justify-center">
            <h1 className="text-center font-normal">Mon profil</h1>
            <div className="xl:w-2/3 xl:space-x-6 xl:flex-row w-full p-6 rounded-lg bg-white flex flex-col items-center justify-center space-y-6">
              <div className="sm:shadow-lg rounded-2xl bg-white sm:p-6 flex flex-col md:flex-row items-center">
                <div className="flex flex-col items-center justify-center md:justify-between space-y-6 mb-10">
                  <div className="flex flex-col items-center justify-start">
                    <Image
                      className="rounded-full h-48 w-48"
                      src={photoUrl}
                      width={100}
                      height={100}
                      alt="Avatar"
                    />
                  </div>
                  <div className="flex flex-col items-center justify-end">
                    <Link
                      className="btn btn-primary mb-5 z-0 py-[14px] md:w-full w-full flex items-center text-center justify-center"
                      href="/modifier-profil"
                      rel=""
                    >
                      Modifier mon profil
                    </Link>
                    <ChangePasswordButton />
                    <ChangePhotoForm
                      id={account.id}
                      onPhotoChange={handlePhotoChange}
                      token={session.accessToken}
                      onPhotoUploadError={onPhotoUploadError}
                    />
                  </div>
                </div>
                <div className="text-left md:ml-6">
                  <h3 className="text-xl font-bold profileTitle mb-6">
                    {account.firstName} {account.lastName}
                  </h3>
                  <p className="text-md text-gray-600 ">
                    {account.attributes.PractionerRole}
                  </p>
                  <p className="text-md text-gray-600 ">
                    Numéro RPPS : {account.attributes.rppsId}
                  </p>
                  <p className="text-md text-gray-600">
                    Numéro de téléphone : {account.attributes.Phone}
                  </p>
                  <p className="text-md text-gray-600">
                    Adresse e-mail : {account.email}
                  </p>
                  <p className="text-md text-gray-600 mt-2 mb-6">
                    Conventionnement :
                  </p>
                  <h3 className="text-xl font-bold profileTitle mb-6">
                    Lieu(x) de consultation
                  </h3>
                  {Object.entries(
                    JSON.parse(
                      account.attributes.Cabinet === undefined
                        ? "{}"
                        : account.attributes.Cabinet,
                    ),
                  ).map(([key, value]) => (
                    <React.Fragment key={key}>
                      <CabinetDisplay id={key} data={value} />
                      <br />
                    </React.Fragment>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
};

export default Profile;
