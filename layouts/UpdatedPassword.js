import { markdownify } from "@lib/utils/textConverter";
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
const NotFound = ({ data }) => {
  const { frontmatter, content } = data;
  const router = useRouter();
  const [currentUrl, setCurrentUrl] = useState('');

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setCurrentUrl(`${window.location.origin}${router.asPath}`);
    }
  }, [router]);

  if(currentUrl.includes("kc_action_status=success")){
    return (
      <section className="section">
        <div className="container pb-32">
          <div className="flex h-[40vh] items-center justify-center">
            <div className="text-center">
              <h1 className="mb-4">Mot de passe modifié avec succès!</h1>
            </div>
          </div>
        </div>
      </section>
    );
  }
  else if(currentUrl.includes("kc_action_status=cancelled")){
    return (
      <section className="section">
        <div className="container pb-32">
          <div className="flex h-[40vh] items-center justify-center">
            <div className="text-center">
              <h1 className="mb-4">Modification du mot de passe annulée</h1>
            </div>
          </div>
        </div>
      </section>
    );
  }else{
    return (
      <section className="section">
        <div className="container pb-32">
          <div className="flex h-[40vh] items-center justify-center">
            <div className="text-center">
              <h1 className="mb-4">Une erreur est survenue.</h1>
            </div>
          </div>
        </div>
      </section>
    );
  }
};

export default NotFound;
