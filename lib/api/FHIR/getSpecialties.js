import axios from "axios";

const getSpecialties = async () => {
  const headersList = {
    Accept: "*/*",
  };

  const reqOptions = {
    url: "https://smt.esante.gouv.fr/fhir/CodeSystem/TRE-R38-SpecialiteOrdinale/",
    method: "GET",
    headers: headersList,
  };

  try {
    const response = await axios.request(reqOptions);
    const codes = response.data.concept;
    const displayArray = codes.map((code) => code.display);
    displayArray.sort();
    return displayArray;
  } catch (error) {
    console.error("Erreur lors de la récupération des spécialités:", error);
    return [];
  }
};

export default getSpecialties;
