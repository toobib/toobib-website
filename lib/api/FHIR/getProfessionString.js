import axios from "axios";

let professionData = null;

export default async function getProfessionString(professionCode) {
   try{
    if (!professionData) {
     professionData = await axios.get("/api/profession");
    }

    const systems = professionData.data.compose.include;
    for (const system of systems) {
	    const codeObj = system.concept.find((concept) => concept.code === professionCode);
	    if (codeObj) {
  	    return codeObj.display;
	    }
    }
    return null;
  }catch(error) {
    console.error("Erreur lors de la récupération des professions:", error);
    return error;
  }
};

