import axios from "axios";

const getPractitionerAndPractitionerRole = async (identifier) => {
  //handle rppsID or Ameli id
  let url;
  if (identifier.includes("-")) {
    url = `https://gateway.api.esante.gouv.fr/fhir/Practitioner?_id=${identifier}&_revinclude=PractitionerRole%3Apractitioner`;
  } else {
    url = `https://gateway.api.esante.gouv.fr/fhir/Practitioner?identifier=${identifier}&_revinclude=PractitionerRole%3Apractitioner`;
  }

  let config = {
    method: "get",
    maxBodyLength: Infinity,
    url: url,
    headers: {
      "ESANTE-API-KEY": process.env.NEXT_PUBLIC_FHIR_KEY,
    },
  };

  return axios
    .request(config)
    .then((response) => {
      // return response.data;
      return {
        data: response.data,
        inpp: identifier,
      }
    })
    .catch((error) => {
      console.log(error);
      return {
        data: null,
        error: {
          message: "An error occurred while fetching data",
          details: error,
        },
      };
    });
};
export default getPractitionerAndPractitionerRole;
