import axios from "axios";

const getOrganizationById = async (_id) => {
  let config = {
    method: "get",
    maxBodyLength: Infinity,
    url: `https://gateway.api.esante.gouv.fr/fhir/Organization?_id=${_id}`,
    headers: {
      "ESANTE-API-KEY": process.env.NEXT_PUBLIC_FHIR_KEY,
    },
  };

  return axios
    .request(config)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
    });
};
export default getOrganizationById;
