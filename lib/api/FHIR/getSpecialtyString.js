import axios from "axios";
const getSpecialtyString = async (specialtyCode) => {
  const headersList = {
    Accept: "*/*",
  };

  const reqOptions = {
    url: "https://smt.esante.gouv.fr/fhir/CodeSystem/TRE-R38-SpecialiteOrdinale",
    method: "GET",
    headers: headersList,
  };

  const response = await axios.request(reqOptions);
  const codes = response.data.concept;
  const codeObj = codes.find((code) => code.code === specialtyCode);
  if (codeObj) {
    return codeObj.display;
  }

  return null;
};
export default getSpecialtyString;
