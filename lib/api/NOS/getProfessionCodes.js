import axios from "axios";
export default async function getProfessionCodes(professionCode) {
  const headersList = {
    Accept: "*/*",
  };

  const reqOptions = {
    url: "https://smt.esante.gouv.fr/fhir/CodeSystem/TRE-G15-ProfessionSante",
    method: "GET",
    headers: headersList,
  };

  try{
    const response = await axios.request(reqOptions);
    const codes = response.data.concept;
    return codes
  }catch(error) {
    console.error("Erreur lors de la récupération des professions:", error);
    return error;
  }
};
