import axios from "axios";

export const getProfessionsJSON = async () => {
  const headersList = {
    Accept: "*/*",
  };

  const reqOptions = {
    url: process.env.NEXT_PUBLIC_SEARCH_API + "/professions",
    method: "GET",
    headers: headersList,
  };

  try {
    const response = await axios.request(reqOptions);
    const json = JSON.parse(response.data)
    return json
  } catch(error) {
    console.error("Erreur lors de la récupération des professions:", error);

    return error;
  }
}

export const getSpecialitiesJSON = async () => {
  const headersList = {
    Accept: "*/*",
  };

  const reqOptions = {
    url: process.env.NEXT_PUBLIC_SEARCH_API + "/specialities",
    method: "GET",
    headers: headersList,
  };

  try {
    const response = await axios.request(reqOptions);
    const json = JSON.parse(response.data)
    console.log(json)
    return json
  } catch(error) {
    console.error("Erreur lors de la récupération des spécialités", error);
    return error;
  }
}

