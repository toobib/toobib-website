import axios from "axios";
import normalize from "@lib/utils/normalize";


const getSearchResults = async (searchParams) => {
  try {
    const response = await axios.get(process.env.NEXT_PUBLIC_SEARCH_API + "/search_practitioner", {
      params: normalize(searchParams),
    });
    return response.data;
  } catch (error) {
    console.error(error);
  }
};

export default getSearchResults;
