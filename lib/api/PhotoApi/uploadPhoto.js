import axios from "axios";
const uploadPhoto = async (photo, token) => {
  try {
    const response = await axios.post(
      process.env.NEXT_PUBLIC_PHOTO_DOMAIN + "/picture/private_or_public/profile",
      photo,
      {
        headers: {
          "Content-Type": "multipart/form-data",
          "Authorization": "Bearer " + token,
        },
      },
    );
    return response;
  } catch (error) {
    return {
      data: null,
      error: {
        message: "An error occurred while uploading data",
        details: error,
      },
    };
  }
};
export default uploadPhoto;
