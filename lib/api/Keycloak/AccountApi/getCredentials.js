import axios from "axios";
import qs from "qs";

const getCredentials = async (token) => {
  let data = qs.stringify({});
  let config = {
    method: "get",
    maxBodyLength: Infinity,
    url: "https://keycloak-test.interhop.org/realms/Toobib/account/credentials",
    headers: {
      Accept: "application/json",
      Authorization: "Bearer " + token,
    },
    data: data,
  };

  return axios
    .request(config)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
    });
};
export default getCredentials;
