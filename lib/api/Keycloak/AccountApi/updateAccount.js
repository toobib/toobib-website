import axios from "axios";

const updateAccount = async (data, accessToken) => {
  let dataToSend = JSON.stringify({
    id: data.id,
    username: data.username,
    firstName: data.firstName,
    lastName: data.lastName,
    email: data.email,
    emailVerified: false,
    userProfileMetadata: {
      attributes: [
        {
          name: "username",
          displayName: "${username}",
          required: true,
          readOnly: true,
          validators: {},
        },
        {
          name: "email",
          displayName: "${email}",
          required: true,
          readOnly: false,
          validators: {
            email: {
              "ignore.empty.value": true,
            },
          },
        },
        {
          name: "firstName",
          displayName: "${firstName}",
          required: true,
          readOnly: false,
          validators: {},
        },
        {
          name: "lastName",
          displayName: "${lastName}",
          required: true,
          readOnly: false,
          validators: {},
        },
      ],
      groups: [],
    },
    attributes: {
      PractionerRole: data.PractionerRole,
      Phone: data.Phone,
      Conventionnement: data.Conventionnement,
      Cabinet: JSON.stringify(data.Cabinet),
      rppsId: data.rppsId,
    },
  });

  let config = {
    method: "post",
    maxBodyLength: Infinity,
    url: "https://keycloak-test.interhop.org/realms/Toobib/account/",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + accessToken,
    },
    data: dataToSend,
  };

  return axios
    .request(config)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
    });
};
export default updateAccount;
