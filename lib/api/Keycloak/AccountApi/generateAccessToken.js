import axios from "axios";
import qs from "qs";

const generateAccessToken = async (username, password) => {
  let data = qs.stringify({
    username: username,
    password: password,
    client_secret: "R8UjYjEGeHKAsCkOoIwBQKJJedJkPJ1s",
    client_id: "account-api",
    grant_type: "password",
  });

  let config = {
    method: "post",
    maxBodyLength: Infinity,
    url: "https://keycloak-test.interhop.org/realms/Toobib/protocol/openid-connect/token",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: data,
  };

  return axios
    .request(config)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
    });
};
export default generateAccessToken;
