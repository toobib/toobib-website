import qs from "qs";
import axios from "axios";

const getAccount = async (token) => {
  let data = qs.stringify({});

  let config = {
    method: "get",
    maxBodyLength: Infinity,
    url: "https://keycloak-test.interhop.org/realms/Toobib/account/",
    headers: {
      Accept: "application/json",
      Authorization: "Bearer " + token,
    },
    data: data,
  };

  return axios
    .request(config)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
    });
};
export default getAccount;
