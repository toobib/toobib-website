import axios from "axios";
import qs from "qs";
const getUserInfo = async (tokenAdmin) => {
  let data = qs.stringify({});

  let config = {
    method: "get",
    maxBodyLength: Infinity,
    url: "https://keycloak-test.interhop.org/realms/Toobib/protocol/openid-connect/userinfo",
    headers: {
      Authorization: "Bearer " + tokenAdmin,
    },
    data: data,
  };

  return axios
    .request(config)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
    });
};
export default getUserInfo;
