import axios from "axios";
import qs from "qs";

const getListOfUsersOfRealms = async (tokenAdmin) => {
  let data = qs.stringify({});

  let config = {
    method: "get",
    maxBodyLength: Infinity,
    url: "https://keycloak-test.interhop.org/admin/realms/Toobib/users",
    headers: {
      Authorization: "Bearer " + tokenAdmin,
    },
    data: data,
  };

  return axios
    .request(config)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
    });
};
export default getListOfUsersOfRealms;
