import getOrganizationById from "@lib/api/FHIR/getOrganizationById";

const extractOrgIdFromFhirPractitionerBundle = (practitionerBundle) => {
  const orgIds = [];
  if (practitionerBundle.total >= 1){
    for (const entry of practitionerBundle.entry) {
      const orgId = entry.resource?.organization?.reference?.split?.("/")?.[1];
      if (orgId) {
        orgIds.push(orgId);
      }
    }
    return orgIds;
  }else{
    return null;
  }
};
const getFormatedOrganizationData = async (orgId) => {
  let fhirData = {};

  if (orgId) {
    const org = await getOrganizationById(orgId);
    let addresse = "";
    org.entry?.[0].resource.address?.[0]._line[0].extension.forEach((extension) => {
      addresse = addresse + " " + extension.valueString;
    });
    fhirData = {
      ...fhirData,
      Lieu: org.entry?.[0].resource.name,
      CodePostal: org.entry?.[0].resource.address?.[0].postalCode,
      Ville: org.entry?.[0].resource.address?.[0].city,
      Adresse: addresse,
      PhoneLieuConsultation:
        org.entry?.[0].resource?.telecom?.length > 0
          ? org.entry[0]?.resource?.telecom[0]?.value ?? ""
          : "",
    };
  }
  return fhirData;
};
export { extractOrgIdFromFhirPractitionerBundle, getFormatedOrganizationData };
