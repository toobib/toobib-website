function normalize(json) {
  const result = {}
  Object.entries(json).map(([key, value]) => {
    if(value !== undefined) {
      result[key] = typeof(value) === 'string' ?  value.toLowerCase().replace(/-/g, '') : value;
    }
  });
  return result;
}

export default normalize;
