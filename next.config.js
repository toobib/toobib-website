/*
	@type {import('next').NextConfig}
*/

const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'photo.toobib.org',
        port: '',
        pathname: '/**',
      },
    ],
  },
  async redirects() {
    return [
      {
        source: "/maquette",
        destination: "https://shorturl.at/s4ddz",
        basePath: false,
        permanent: false,
      },
    ];
  },
  webpack: (config, { isServer }) => {
    config.module.rules.push({
      test: /\.csv$/,
      loader: 'csv-loader',
      options: {
        dynamicTyping: true,
        header: true,
        skipEmptyLines: true,
        delimiter: ';',  // Utilisation du point-virgule comme délimiteur
      },
    });

    return config;
  },
};

module.exports = nextConfig;
