# Page d'accueil Toobib réalisée avec Next.js

Toobib est une plateforme web permettant aux professionnel.le.s de santé de d'accéder à des services numériques libres.


## Conception

Nous utilisons penpot pour concevoir la [prochaine version de toobib](https://penpot.interhop.org/#/view/1f43cc2f-6ecb-8131-8002-6fca9a74db2a?section=interactions&index=0&share-id=7e202e91-92ad-8006-8003-19ade857756d).

Aujourd'hui, nous utilisons le modèle [bigspring-light](https://github.com/themefisher/bigspring-light-nextjs).

## Installation

Après avoir téléchargé le modèle, vous devez installer quelques prérequis. Vous pouvez ensuite l'exécuter sur votre hôte local. Vous pouvez consulter le fichier package.json pour voir quels scripts sont inclus.

### cloner le répertoire

```sh
git clone https://framagit.org/toobib/toobib-landing-page-with-reactjs.git
```

### Installer les prérequis (une fois par machine)

**Installation de node:** [Installer node js](https://nodejs.org/en/download/) [Version LTS recommandée].

### Configuration locale

* Installer les dépendances

```
npm install
```

* Configurer le fichier .env.local

Afin de s'assurer du bon fonctionnement de toutes les fonctionnalités de Toobib, on s'assure de la bonne configuration du fichier .env.local contenant les variables d'environnement.

```.env.local
#ONLINE KEYCLOAK (Utilisé en environnement de test/de production)
KEYCLOAK_CLIENT_ID="CLIENT_NAME" (à créer dans le panel d'administration Keycloak (Client > Create Client))
KEYCLOAK_CLIENT_SECRET="CLIENT_KEY" (à récupérer dans le panel d'administration Keycloak (Client > CLIENT_NAME > Credentials > Client Secret)
KEYCLOAK_ISSUER="<KEYCLOAK_SERVER_URL>/realms/<REALM_NAME>"(Copy the url of your keycloak provider in Keycloak_server_url and copy the realm name in REALM_NAME par exemple dans la redirection vers la fonction Keycloak de changement de mot de passe)


NEXTAUTH_URL="<PRODUCTION_URL"
NEXTAUTH_SECRET="<SECRET_FOR_NEXT>"

#FHIR KEY
NEXT_PUBLIC_FHIR_KEY="<SECRET_FOR_FHIR>"
NEXT_PUBLIC_CLIENT_ID="CLIENT_NAME" (Variable d'environnement publique utilisée par exemple dans la configuration de la redirection du changement de mot de passe)

#REDIRECT URL's (à valider pour chaque client Keycloak sur le panel d'administration Keycloak (Clients > CLIENT_NAME > Settings > Valid redirect URIs))
NEXT_PUBLIC_ISSUER="https://<KEYCLOAK_SERVER_URL>/realms/<REALM_NAME>" (Variable d'environnement publique de l'URL utilisée par exemple dans la redirection vers la fonction Keycloak de changement de mot de passe)
NEXT_PUBLIC_KEYCLOAK_REDIRECT_URL_PWD=NEXTAUTH_URL + "/mdp-modifie" (Variable d'environnement publique de l'URL de redirection vers l'application après un changement de mot de passe)

NEXT_PUBLIC_KEYCLOAK_REDIRECT_URL_LOGOUT=NEXT_AUTH_URL
NEXT_PUBLIC_PHOTO_DOMAIN="<PHOTO_API_URL>"
NEXT_PUBLIC_SEARCH_API="<SEARCH_API_URL>"
```

* Exécuter localement

```
npm run dev
```

Après cela, il ouvrira un aperçu du modèle dans votre navigateur par défaut, surveillera les modifications apportées aux fichiers sources et rechargera le navigateur en direct lorsque les modifications seront enregistrées.

## Construction de la production

Après avoir terminé toutes les personnalisations, vous pouvez créer une version de production en lançant cette commande.

```
npm run build
npm run start
```
## Structure du projet

```
.
├── pages/                      # pages' routing
│   ├── blogs/                  # blog-related pages' routing
│   ├── api/
│   │   └── auth/               # authentication API routes for KC
│   ├── 404.js                  # 404 error page
│   ├── [regular].js            # Dynamic route
│   ├── app.js                  # Main application page
│   ├── document.js             # Custom document page for server-side rendering
│   └── index.js                # Homepage
├── layouts/                    # How the content will be displayed on the website
├── content/                    # content of the website
├── asset/                      
│   └── Luciole/                # Luciole font
├── config/                     # configuration files
├── lib/                        # utility fonctions
├── public/                     # publicly accessible static assets
├── styles/                     # SCSS stylesheets
├── .editorconfig               # [editorconfig](https://editorconfig.org/) help maintain consistent coding style between editor
├── .eslintrc.json              # ESLint configuration file
├── .gitignore                  # Git ignore file
├── README.md                   # Project README file
├── jsconfig.json               # JavaScript configuration file
├── next.config.js              # Next.js configuration file
├── package.json                # List of our dependencies
├── postcss.config.js           # CSS Optimization with javascript
└── tailwind.config.js          # Tailwind theme CSS customization
```

###  Ajouter une page

Pour créer une nouvelle page au projet, vous devez l'ajouter aux routes disponibles dans le fichier [regular].js. Puis créer un layout dans le dossier "layouts". Enfin  dans le dossier "content/", créez un document markdown lié au layout créé.

### Ajouter un article de blog

Pour ajouter un nouvel article de blog, utilisez le fichier template caché dans le dossier '/content/blogs/': Copiez le, renommez le fichier et apportez lui les information souhaitées au format Markdown.


## Signaler des problèmes

Si vous souhaitez poser une question, envoyez-la nous via notre [page de contact](https://toobib.org/contact).

## Licence
Notre code est publié sous la licence [MIT](https://en.wikipedia.org/wiki/MIT_License).

**Licence des images:** Les images sont uniquement destinées à la démonstration. Elles ont leur propre licence, nous n'avons pas la permission de partager ces images.
